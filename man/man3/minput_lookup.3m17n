.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "minput_lookup" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
minput_lookup \- Look up a text produced in the input context. 


.SH SYNOPSIS
int
\fBminput_lookup\fP (\fBMInputContext\fP *
\fIic\fP, \fBMSymbol\fP
\fIkey\fP, void *
\fIarg\fP, \fBMText\fP *
\fImt\fP)


.SH DESCRIPTION
Look up a text produced in the input context. The
.ft B
minput_lookup()
.ft R
function looks up a text in the input context
.ft B
ic\fP.
.ft R
.ft B
key
.ft R
must be identical to the one that was used in the previous call of
.ft B
minput_filter()\fP.
.ft R
.PP
If a text was produced by the input method, it is concatenated to M\-text
.ft B
mt\fP.
.ft R
.PP
This function calls
.ft B
MInputDriver::lookup\fP.
.ft R
.PP
.SH RETURN VALUE
.PP
.RS 4
If
.ft B
key
.ft R
was correctly handled by the input method, this function returns 0. Otherwise, it returns \-1, even though some text might be produced in
.ft B
mt\fP.
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
