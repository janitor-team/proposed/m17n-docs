.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "Property List" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
Property_List \- Property List objects and API for them.  

.SS "Typedefs"

.in +1c
.ti -1c
.RI "typedef struct \fBMPlist\fP \fBMPlist\fP"
.br
.RI "\fIType of property list objects. \fP"
.in -1c
.SS "Functions"

.in +1c
.ti -1c
.RI "\fBMPlist\fP * \fBmplist_deserialize\fP (\fBMText\fP *mt)"
.br
.RI "\fIGenerate a property list by deserializing an M-text. \fP"
.ti -1c
.RI "\fBMPlist\fP * \fBmplist\fP (void)"
.br
.RI "\fICreate a property list object. \fP"
.ti -1c
.RI "\fBMPlist\fP * \fBmplist_copy\fP (\fBMPlist\fP *plist)"
.br
.RI "\fICopy a property list. \fP"
.ti -1c
.RI "\fBMPlist\fP * \fBmplist_put\fP (\fBMPlist\fP *plist, \fBMSymbol\fP key, void *val)"
.br
.RI "\fISet the value of a property in a property list. \fP"
.ti -1c
.RI "void * \fBmplist_get\fP (\fBMPlist\fP *plist, \fBMSymbol\fP key)"
.br
.RI "\fIGet the value of a property in a property list. \fP"
.ti -1c
.RI "\fBMPlist\fP * \fBmplist_put_func\fP (\fBMPlist\fP *plist, \fBMSymbol\fP key, \fBM17NFunc\fP func)"
.br
.RI "\fISet the value (function pointer) of a property in a property list. \fP"
.ti -1c
.RI "\fBM17NFunc\fP \fBmplist_get_func\fP (\fBMPlist\fP *plist, \fBMSymbol\fP key)"
.br
.RI "\fIGet the value (function pointer) of a property in a property list. \fP"
.ti -1c
.RI "\fBMPlist\fP * \fBmplist_add\fP (\fBMPlist\fP *plist, \fBMSymbol\fP key, void *val)"
.br
.RI "\fIAdd a property at the end of a property list. \fP"
.ti -1c
.RI "\fBMPlist\fP * \fBmplist_push\fP (\fBMPlist\fP *plist, \fBMSymbol\fP key, void *val)"
.br
.RI "\fIAdd a property at the beginning of a property list. \fP"
.ti -1c
.RI "void * \fBmplist_pop\fP (\fBMPlist\fP *plist)"
.br
.RI "\fIRemove a property at the beginning of a property list. \fP"
.ti -1c
.RI "\fBMPlist\fP * \fBmplist_find_by_key\fP (\fBMPlist\fP *plist, \fBMSymbol\fP key)"
.br
.RI "\fIFind a property of a specific key in a property list. \fP"
.ti -1c
.RI "\fBMPlist\fP * \fBmplist_find_by_value\fP (\fBMPlist\fP *plist, void *val)"
.br
.RI "\fIFind a property of a specific value in a property list. \fP"
.ti -1c
.RI "\fBMPlist\fP * \fBmplist_next\fP (\fBMPlist\fP *plist)"
.br
.RI "\fIReturn the next sublist of a property list. \fP"
.ti -1c
.RI "\fBMPlist\fP * \fBmplist_set\fP (\fBMPlist\fP *plist, \fBMSymbol\fP key, void *val)"
.br
.RI "\fISet the first property in a property list. \fP"
.ti -1c
.RI "int \fBmplist_length\fP (\fBMPlist\fP *plist)"
.br
.RI "\fIReturn the length of a property list. \fP"
.ti -1c
.RI "\fBMSymbol\fP \fBmplist_key\fP (\fBMPlist\fP *plist)"
.br
.RI "\fIReturn the key of the first property in a property list. \fP"
.ti -1c
.RI "void * \fBmplist_value\fP (\fBMPlist\fP *plist)"
.br
.RI "\fIReturn the value of the first property in a property list. \fP"
.in -1c
.SS "Variables"

.in +1c
.ti -1c
.RI "\fBMSymbol\fP \fBMinteger\fP"
.br
.RI "\fISymbol whose name is 'integer'. \fP"
.ti -1c
.RI "\fBMSymbol\fP \fBMplist\fP"
.br
.RI "\fISymbol whose name is 'plist'. \fP"
.ti -1c
.RI "\fBMSymbol\fP \fBMtext\fP"
.br
.RI "\fISymbol whose name is 'mtext'. \fP"
.in -1c
.SH "Detailed Description"
.PP 
Property List objects and API for them. 

A \fIproperty\fP \fIlist\fP (or \fIplist\fP for short) is a list of zero or more properties. A property consists of a \fIkey\fP and a \fIvalue\fP, where key is a symbol and value is anything that can be cast to \fC(void *)\fP.
.PP
If the key of a property is a \fImanaging\fP \fIkey\fP, its \fIvalue\fP is a \fImanaged\fP \fIobject\fP. A property list itself is a managed objects.
.PP
If each key of a plist is one of \fBMsymbol\fP, \fBMtext\fP, \fBMinteger\fP, and \fBMplist\fP, the plist is called as \fIwell\-formed\fP and represented by the following notation in the documentation.
.PP
.PP
.nf

      PLIST ::= '(' ELEMENT * ')'

      ELEMENT ::= INTEGER | SYMBOL | M\-TEXT | PLIST

      M\-TEXT ::= '"' text data ... '"'
.fi
.PP
.PP
For instance, if a plist has four elements; integer \-20, symbol of name 'sym', M\-text of contents 'abc', and plist of integer 10 and symbol of name 'another\-symbol', it is represented as this:
.PP
(\-20 sym 'abc' (10 another\-symbol)) 
.SH "Typedef Documentation"
.PP 
.SS "typedef struct \fBMPlist\fP \fBMPlist\fP"
.PP
Type of property list objects. The type \fBMPlist\fP is for a \fIproperty\fP \fIlist\fP object. Its internal structure is concealed from application programs. 
.SH "Variable Documentation"
.PP 
.SS "\fBMSymbol\fP \fBMinteger\fP"
.PP
Symbol whose name is 'integer'. The symbol \fCMinteger\fP has the name \fC'integer'\fP. The value of a property whose key is \fCMinteger\fP must be an integer. 
.SS "\fBMSymbol\fP \fBMplist\fP"
.PP
Symbol whose name is 'plist'. The symbol \fCMplist\fP has the name \fC'plist'\fP. It is a managing key. A value of a property whose key is \fCMplist\fP must be a plist. 
.SS "\fBMSymbol\fP \fBMtext\fP"
.PP
Symbol whose name is 'mtext'. The symbol \fCMtext\fP has the name \fC'mtext'\fP. It is a managing key. A value of a property whose key is \fCMtext\fP must be an M\-text. 
.SH "Author"
.PP 
Generated automatically by Doxygen for The m17n Library from the source code.
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
