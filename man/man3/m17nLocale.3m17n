.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "Locale" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
Locale \- Locale objects and API for them.  

.SS "Typedefs"

.in +1c
.ti -1c
.RI "typedef struct \fBMLocale\fP \fBMLocale\fP"
.br
.RI "\fI\fCstruct\fP \fCMLocale\fP. \fP"
.in -1c
.SS "Functions"

.in +1c
.ti -1c
.RI "\fBMLocale\fP * \fBmlocale_set\fP (int category, const char *name)"
.br
.RI "\fISet the current locale. \fP"
.ti -1c
.RI "\fBMSymbol\fP \fBmlocale_get_prop\fP (\fBMLocale\fP *locale, \fBMSymbol\fP key)"
.br
.RI "\fIGet the value of a locale property. \fP"
.ti -1c
.RI "int \fBmtext_ftime\fP (\fBMText\fP *mt, const char *format, const struct tm *tm, \fBMLocale\fP *locale)"
.br
.RI "\fIFormat date and time. \fP"
.ti -1c
.RI "\fBMText\fP * \fBmtext_getenv\fP (const char *name)"
.br
.RI "\fIGet an environment variable. \fP"
.ti -1c
.RI "int \fBmtext_putenv\fP (\fBMText\fP *mt)"
.br
.RI "\fIChange or add an environment variable. \fP"
.ti -1c
.RI "int \fBmtext_coll\fP (\fBMText\fP *mt1, \fBMText\fP *mt2)"
.br
.RI "\fICompare two M-texts using the current locale. \fP"
.in -1c
.SS "Variables"

.in +1c
.ti -1c
.RI "\fBMSymbol\fP \fBMterritory\fP"
.br
.ti -1c
.RI "\fBMSymbol\fP \fBMmodifier\fP"
.br
.ti -1c
.RI "\fBMSymbol\fP \fBMcodeset\fP"
.br
.in -1c
.SH "Detailed Description"
.PP 
Locale objects and API for them. 

The m17n library represents locale related information as objects of type \fBMLocale\fP. 
.SH "Typedef Documentation"
.PP 
.SS "typedef struct \fBMLocale\fP \fBMLocale\fP"
.PP
\fCstruct\fP \fCMLocale\fP. The structure \fCMLocale\fP is used to hold information about name, language, territory, modifier, codeset, and the corresponding coding system of locales.
.PP
The contents of this structure are implementation dependent. Its internal structure is concealed from application programs.
.PP

\fBSEE ALSO\fp
.RS 4
\fBmlocale_get_prop()\fP 
.RE
.PP

.SH "Variable Documentation"
.PP 
.SS "\fBMSymbol\fP \fBMterritory\fP"The symbol whose name is 'territory'. 
.SS "\fBMSymbol\fP \fBMmodifier\fP"The symbol whose name is 'modifier'. 
.SS "\fBMSymbol\fP \fBMcodeset\fP"The symbol whose name is 'codeset'. 
.SH "Author"
.PP 
Generated automatically by Doxygen for The m17n Library from the source code.
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
