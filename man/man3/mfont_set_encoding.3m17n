.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mfont_set_encoding" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mfont_set_encoding \- Set encoding of a font. 


.SH SYNOPSIS
int
\fBmfont_set_encoding\fP (\fBMFont\fP *
\fIfont\fP, \fBMSymbol\fP
\fIencoding_name\fP, \fBMSymbol\fP
\fIrepertory_name\fP)


.SH DESCRIPTION
Set encoding of a font. The
.ft B
mfont_set_encoding()
.ft R
function sets the encoding information of font
.ft B
font\fP.
.ft R
.PP
\fBencoding_name
.ft R
is a symbol representing a charset that has the same encoding as the font.
.PP
\fBrepertory_name
.ft R
is
.ft C
Mnil
.ft R
or a symbol representing a charset that has the same repertory as the font. If it is
.ft C
Mnil\fP,
.ft R
whether a specific character is supported by the font is asked to each font driver.
.PP
.SH RETURN VALUE
.PP
.RS 4
If the operation was successful, this function returns 0. Otherwise it returns \-1 and assigns an error code to the external variable
.ft B
merror_code\fP.
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
