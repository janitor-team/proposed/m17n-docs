.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mtext_dup" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mtext_dup \- Create a copy of an M\-text. 


.SH SYNOPSIS
\fBMText\fP*
\fBmtext_dup\fP (\fBMText\fP *
\fImt\fP)


.SH DESCRIPTION
Create a copy of an M\-text. The
.ft B
mtext_dup()
.ft R
function creates a copy of M\-text
.ft B
mt
.ft R
while inheriting all the text properties of
.ft B
mt\fP.
.ft R
.PP
.SH RETURN VALUE
.PP
.RS 4
This function returns a pointer to the created copy.
.RE
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBmtext_duplicate()
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
