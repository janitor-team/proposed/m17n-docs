.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mtext_pbrk" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mtext_pbrk \- Search an M\-text for any of a set of characters. 


.SH SYNOPSIS
int
\fBmtext_pbrk\fP (\fBMText\fP *
\fImt\fP, \fBMText\fP *
\fIaccept\fP)


.SH DESCRIPTION
Search an M\-text for any of a set of characters. The
.ft B
mtext_pbrk()
.ft R
function locates the first occurrence in M\-text
.ft B
mt1
.ft R
of any of the characters in M\-text
.ft B
mt2\fP.
.ft R
.PP
.SH RETURN VALUE
.PP
.RS 4
This function returns the position in
.ft B
mt1
.ft R
of the found character. If no such character is found, it returns \-1. 
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
