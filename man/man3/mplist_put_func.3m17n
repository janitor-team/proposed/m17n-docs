.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mplist_put_func" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mplist_put_func \- Set the value (function pointer) of a property in a property list. 


.SH SYNOPSIS
\fBMPlist\fP*
\fBmplist_put_func\fP (\fBMPlist\fP *
\fIplist\fP, \fBMSymbol\fP
\fIkey\fP, \fBM17NFunc\fP
\fIfunc\fP)


.SH DESCRIPTION
Set the value (function pointer) of a property in a property list. The
.ft B
mplist_put_func()
.ft R
function is similar to
.ft B
mplist_put()
.ft R
but for setting function pointer
.ft B
func
.ft R
in property list
.ft B
plist
.ft R
for key
.ft B
key\fP.
.ft R
.ft B
key
.ft R
must not be a managing key.
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBmplist_put()\fP,
.ft R
.ft B
M17N_FUNC()
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
