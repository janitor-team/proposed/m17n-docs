.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "minput_set_spot" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
minput_set_spot \- Set the spot of the input context. 


.SH SYNOPSIS
void
\fBminput_set_spot\fP (\fBMInputContext\fP *
\fIic\fP, int
\fIx\fP, int
\fIy\fP, int
\fIascent\fP, int
\fIdescent\fP, int
\fIfontsize\fP, \fBMText\fP *
\fImt\fP, int
\fIpos\fP)


.SH DESCRIPTION
Set the spot of the input context. The
.ft B
minput_set_spot()
.ft R
function sets the spot of input context
.ft B
ic
.ft R
to coordinate (\fBx\fP,
.ft R
.ft B
y
.ft R
) with the height specified by
.ft B
ascent
.ft R
and
.ft B
descent\fP.
.ft R
The semantics of these values depends on the input method driver.
.PP
For instance, a driver designed to work in a CUI environment may use
.ft B
x
.ft R
and
.ft B
y
.ft R
as the column\- and row numbers, and may ignore
.ft B
ascent
.ft R
and
.ft B
descent\fP.
.ft R
A driver designed to work in a window system may interpret
.ft B
x
.ft R
and
.ft B
y
.ft R
as the pixel offsets relative to the origin of the client window, and may interpret
.ft B
ascent
.ft R
and
.ft B
descent
.ft R
as the ascent\- and descent pixels of the line at (\fBx\fP.
.ft R
.ft B
y
.ft R
).
.PP
\fBfontsize
.ft R
specifies the fontsize of preedit text in 1/10 point.
.PP
\fBmt
.ft R
and
.ft B
pos
.ft R
are the M\-text and the character position at the spot.
.ft B
mt
.ft R
may be
.ft C
NULL\fP,
.ft R
in which case, the input method cannot get information about the text around the spot. 
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
