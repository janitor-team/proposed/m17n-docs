.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "minput_save_config" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
minput_save_config \- Save configurations in per\-user customization file. 


.SH SYNOPSIS
int
\fBminput_save_config\fP (void)


.SH DESCRIPTION
Save configurations in per\-user customization file. The
.ft B
minput_save_config()
.ft R
function saves the configurations done so far in the current session into the per\-user customization file.
.PP
.SH RETURN VALUE
.PP
.RS 4

.RE
.PP
If the operation was successful, 1 is returned. If the per\-user customization file is currently locked, 0 is returned. In that case, the caller may wait for a while and try again. If the configuration file is not writable, \-1 is returned. In that case, the caller may check the name of the file by calling
.ft B
minput_config_file()\fP,
.ft R
make it writable if possible, and try again.
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBminput_config_file()
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
