.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mconv_getc" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mconv_getc \- Read a character via a code converter. 


.SH SYNOPSIS
int
\fBmconv_getc\fP (\fBMConverter\fP *
\fIconverter\fP)


.SH DESCRIPTION
Read a character via a code converter. The
.ft B
mconv_getc()
.ft R
function reads one character from the buffer area or the stream that is currently bound to code converter
.ft B
converter\fP.
.ft R
The decoder of
.ft B
converter
.ft R
is used to decode the byte sequence. The internal status of
.ft B
converter
.ft R
is updated appropriately.
.PP
.SH RETURN VALUE
.PP
.RS 4
If the operation was successful,
.ft B
mconv_getc()
.ft R
returns the character read in. If the input source reaches EOF, it returns
.ft C
EOF
.ft R
without changing the external variable
.ft B
merror_code\fP.
.ft R
If an error is detected, it returns
.ft C
EOF
.ft R
and assigns an error code to
.ft B
merror_code\fP.
.ft R
.RE
.PP
.SH ERRORS
.PP
.RS 4
\fCMERROR_CODING
.ft R
.RE
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBmconv_ungetc()\fP,
.ft R
.ft B
mconv_putc()\fP,
.ft R
.ft B
mconv_gets()
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
