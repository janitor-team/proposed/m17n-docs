.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mtext_tok" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mtext_tok \- Look for a token in an M\-text. 


.SH SYNOPSIS
\fBMText\fP*
\fBmtext_tok\fP (\fBMText\fP *
\fImt\fP, \fBMText\fP *
\fIdelim\fP, int *
\fIpos\fP)


.SH DESCRIPTION
Look for a token in an M\-text. The
.ft B
mtext_tok()
.ft R
function searches a token that firstly occurs after position
.ft B
pos
.ft R
in M\-text
.ft B
mt\fP.
.ft R
Here, a token means a substring each of which does not appear in M\-text
.ft B
delim\fP.
.ft R
Note that the type of
.ft B
pos
.ft R
is not
.ft C
int
.ft R
but pointer to
.ft C
int\fP.
.ft R
.PP
.SH RETURN VALUE
.PP
.RS 4
If a token is found,
.ft B
mtext_tok()
.ft R
copies the corresponding part of
.ft B
mt
.ft R
and returns a pointer to the copy. In this case,
.ft B
pos
.ft R
is set to the end of the found token. If no token is found, it returns
.ft C
NULL
.ft R
without changing the external variable
.ft B
merror_code\fP.
.ft R
If an error is detected, it returns
.ft C
NULL
.ft R
and assigns an error code to the external variable
.ft B
merror_code\fP.
.ft R
.RE
.PP
.SH ERRORS
.PP
.RS 4
\fCMERROR_RANGE
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
