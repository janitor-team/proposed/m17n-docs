.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mframe" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mframe \- Create a new frame. 


.SH SYNOPSIS
\fBMFrame\fP*
\fBmframe\fP (\fBMPlist\fP *
\fIplist\fP)


.SH DESCRIPTION
Create a new frame. The
.ft B
mframe()
.ft R
function creates a new frame with parameters listed in
.ft B
plist
.ft R
which may be
.ft C
NULL\fP.
.ft R
.PP
The recognized keys in
.ft B
plist
.ft R
are window system dependent.
.PP
The following key is always recognized.
.PP
.PD 0
.IP "\(bu" 2
\fBMdevice\fP,
.ft R
the value must be one of
.ft B
Mx\fP,
.ft R
.ft B
Mgd\fP,
.ft R
and
.ft B
Mnil\fP.
.ft R
.PP
If the value is
.ft B
Mx\fP,
.ft R
the frame is for X Window System. The argument
.ft B
MDrawWindow
.ft R
specified together with the frame must be of type
.ft C
Window\fP.
.ft R
The frame is both readable and writable, thus all GUI functions can be used.
.PP
If the value is
.ft B
Mgd\fP,
.ft R
the frame is for an image object of GD library. The argument
.ft B
MDrawWindow
.ft R
specified together with the frame must be of type
.ft C
gdImagePtr\fP.
.ft R
The frame is writable only, thus functions minput_XXX can't be used for the frame.
.PP
If the value is
.ft B
Mnil\fP,
.ft R
the frame is for a null device. The frame is not writable nor readable, thus functions mdraw_XXX that require the argument
.ft B
MDrawWindow
.ft R
and functions minput_XXX can't be used for the frame.
.PP

.IP "\(bu" 2
\fBMface\fP,
.ft R
the value must be a pointer to
.ft B
MFace\fP.
.ft R
.PP
The value is used as the default face of the frame.
.PP

.PP
.PP
In addition, if the value of the key
.ft B
Mdevice
.ft R
is
.ft B
Mx\fP,
.ft R
the following keys are recognized. They are to specify the root window and the depth of drawables that can be used with the frame.
.PP
.PD 0
.IP "\(bu" 2
\fBMdrawable\fP,
.ft R
the value type must be
.ft C
Drawable\fP.
.ft R
.PP
A parameter of key
.ft B
Mdisplay
.ft R
must also be specified. The created frame can be used for drawables whose root window and depth are the same as those of the specified drawable on the specified display.
.PP
When this parameter is specified, the parameter of key
.ft B
Mscreen
.ft R
is ignored.
.PP

.IP "\(bu" 2
\fBMwidget\fP,
.ft R
the value type must be
.ft C
Widget\fP.
.ft R
.PP
The created frame can be used for drawables whose root window and depth are the same as those of the specified widget.
.PP
If a parameter of key
.ft B
Mface
.ft R
is not specified, the default face is created from the resources of the widget.
.PP
When this parameter is specified, the parameters of key
.ft B
Mdisplay\fP,
.ft R
.ft B
Mscreen\fP,
.ft R
.ft B
Mdrawable\fP,
.ft R
.ft B
Mdepth
.ft R
are ignored.
.PP

.IP "\(bu" 2
\fBMdepth\fP,
.ft R
the value type must be
.ft C
unsigned\fP.
.ft R
.PP
The created frame can be used for drawables of the specified depth.
.PP

.IP "\(bu" 2
\fBMscreen\fP,
.ft R
the value type must be
.ft C
(Screen *)\fP.
.ft R
.PP
The created frame can be used for drawables whose root window is the same as the root window of the specified screen, and depth is the same at the default depth of the screen.
.PP
When this parameter is specified, parameter of key
.ft B
Mdisplay
.ft R
is ignored.
.PP

.IP "\(bu" 2
\fBMdisplay\fP,
.ft R
the value type must be
.ft C
(Display *)\fP.
.ft R
.PP
The created frame can be used for drawables whose root window is the same as the root window for the default screen of the display, and depth is the same as the default depth of the screen.
.PP

.IP "\(bu" 2
\fBMcolormap\fP,
.ft R
the value type must be
.ft C
(Colormap)\fP.
.ft R
.PP
The created frame uses the specified colormap.
.PP

.IP "\(bu" 2
\fBMfont\fP,
.ft R
the value must be
.ft B
Mx\fP,
.ft R
.ft B
Mfreetype\fP,
.ft R
or
.ft B
Mxft\fP.
.ft R
.PP
The created frame uses the specified font backend. The value
.ft B
Mx
.ft R
instructs to use X core fonts,
.ft B
Mfreetype
.ft R
to use local fonts supported by FreeType fonts, and
.ft B
Mxft
.ft R
to use local fonts via Xft library. You can specify this parameter more than once with different values if you want to use multiple font backends. This is ignored if the specified font backend is not supported on the device.
.PP
When this parameter is not specified, all font backend supported on the device are used.
.PP

.PP
.PP
.SH RETURN VALUE
.PP
.RS 4
If the operation was successful,
.ft B
mframe()
.ft R
returns a pointer to a newly created frame. Otherwise, it returns
.ft C
NULL\fP.
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
