.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mtext_property" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mtext_property \- Create a text property. 


.SH SYNOPSIS
\fBMTextProperty\fP*
\fBmtext_property\fP (\fBMSymbol\fP
\fIkey\fP, void *
\fIval\fP, int
\fIcontrol_bits\fP)


.SH DESCRIPTION
Create a text property. The
.ft B
mtext_property()
.ft R
function returns a newly allocated text property whose key is
.ft B
key
.ft R
and value is
.ft B
val\fP.
.ft R
The created text property is not attached to any M\-text, i.e. it is detached.
.PP
\fBcontrol_bits
.ft R
must be 0 or logical OR of
.ft C
enum
.ft R
.ft C
MTextPropertyControl\fP.
.ft R
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
