.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mtext_chr" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mtext_chr \- Return the position of the first occurrence of a character in an M\-text. 


.SH SYNOPSIS
int
\fBmtext_chr\fP (\fBMText\fP *
\fImt\fP, int
\fIc\fP)


.SH DESCRIPTION
Return the position of the first occurrence of a character in an M\-text. The
.ft B
mtext_chr()
.ft R
function searches M\-text
.ft B
mt
.ft R
for character
.ft B
c\fP.
.ft R
The search starts from the beginning of
.ft B
mt
.ft R
and goes toward the end.
.PP
.SH RETURN VALUE
.PP
.RS 4
If
.ft B
c
.ft R
is found,
.ft B
mtext_chr()
.ft R
returns its position; otherwise it returns \-1.
.RE
.PP
.SH ERRORS
.PP
.RS 4
\fCMERROR_RANGE
.ft R
.RE
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBmtext_rchr()\fP,
.ft R
.ft B
mtext_character()
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
