.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "minput_filter" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
minput_filter \- Filter an input key. 


.SH SYNOPSIS
int
\fBminput_filter\fP (\fBMInputContext\fP *
\fIic\fP, \fBMSymbol\fP
\fIkey\fP, void *
\fIarg\fP)


.SH DESCRIPTION
Filter an input key. The
.ft B
minput_filter()
.ft R
function filters input key
.ft B
key
.ft R
according to input context
.ft B
ic\fP,
.ft R
and calls callback functions corresponding to
.ft B
Minput_preedit_draw\fP,
.ft R
.ft B
Minput_status_draw\fP,
.ft R
and
.ft B
Minput_candidates_draw
.ft R
if the preedit text, the status, and the current candidate are changed respectively.
.PP
To make the input method commit the current preedit text (if any) and shift to the initial state, call this function with
.ft B
Mnil
.ft R
as
.ft B
key\fP.
.ft R
.PP
To inform the input method about the focus\-out event, call this function with
.ft B
Minput_focus_out
.ft R
as
.ft B
key\fP.
.ft R
.PP
To inform the input method about the focus\-in event, call this function with
.ft B
Minput_focus_in
.ft R
as
.ft B
key\fP.
.ft R
.PP
To inform the input method about the focus\-move event (i.e. input spot change within the same input context), call this function with
.ft B
Minput_focus_move
.ft R
as
.ft B
key\fP.
.ft R
.PP
.SH RETURN VALUE
.PP
.RS 4
If
.ft B
key
.ft R
is filtered out, this function returns 1. In that case, the caller should discard the key. Otherwise, it returns 0, and the caller should handle the key, for instance, by calling the function
.ft B
minput_lookup()
.ft R
with the same key. 
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
