.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mtext_data" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mtext_data \- Get information about the text data in M\-text. 


.SH SYNOPSIS
void*
\fBmtext_data\fP (\fBMText\fP *
\fImt\fP, enum \fBMTextFormat\fP *
\fIfmt\fP, int *
\fInunits\fP, int *
\fIpos_idx\fP, int *
\fIunit_idx\fP)


.SH DESCRIPTION
Get information about the text data in M\-text. The
.ft B
mtext_data()
.ft R
function returns a pointer to the text data of M\-text
.ft B
mt\fP.
.ft R
If
.ft B
fmt
.ft R
is not NULL, the format of the text data is stored in it. If
.ft B
nunits
.ft R
is not NULL, the number of units of the text data is stored in it.
.PP
If
.ft B
pos_idx
.ft R
is not NULL and it points to a non\-negative number, what it points to is a character position. In this case, the return value is a pointer to the text data of a character at that position.
.PP
Otherwise, if
.ft B
unit_idx
.ft R
is not NULL, it points to a unit position. In this case, the return value is a pointer to the text data of a character containing that unit.
.PP
The character position and unit position of the return value are stored in
.ft B
pos_idx
.ft R
and
.ft B
unit_dix
.ft R
respectively if they are not NULL.
.PP
.PD 0
.IP "\(bu" 2
If the format of the text data is MTEXT_FORMAT_US_ASCII or MTEXT_FORMAT_UTF_8, one unit is unsigned char.
.PP

.IP "\(bu" 2
If the format is MTEXT_FORMAT_UTF_16LE or MTEXT_FORMAT_UTF_16BE, one unit is unsigned short.
.PP

.IP "\(bu" 2
If the format is MTEXT_FORMAT_UTF_32LE or MTEXT_FORMAT_UTF_32BE, one unit is unsigned int.
.PP

.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
