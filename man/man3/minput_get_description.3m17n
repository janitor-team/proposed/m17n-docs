.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "minput_get_description" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
minput_get_description \- Get description text of an input method. 


.SH SYNOPSIS
\fBMText\fP*
\fBminput_get_description\fP (\fBMSymbol\fP
\fIlanguage\fP, \fBMSymbol\fP
\fIname\fP)


.SH DESCRIPTION
Get description text of an input method. The
.ft B
minput_get_description()
.ft R
function returns an M\-text that describes the input method specified by
.ft B
language
.ft R
and
.ft B
name\fP.
.ft R
.PP
.SH RETURN VALUE
.PP
.RS 4
If the specified input method has a description text, a pointer to
.ft B
MText
.ft R
is returned. The caller has to free it by
.ft B
m17n_object_unref()\fP.
.ft R
If the input method does not have a description text,
.ft C
NULL
.ft R
is returned. 
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
