.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mtext_ncmp" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mtext_ncmp \- Compare initial parts of two M\-texts character\-by\-character. 


.SH SYNOPSIS
int
\fBmtext_ncmp\fP (\fBMText\fP *
\fImt1\fP, \fBMText\fP *
\fImt2\fP, int
\fIn\fP)


.SH DESCRIPTION
Compare initial parts of two M\-texts character\-by\-character. The
.ft B
mtext_ncmp()
.ft R
function is similar to
.ft B
mtext_cmp()\fP,
.ft R
but compares at most
.ft B
n
.ft R
characters from the beginning.
.PP
.SH RETURN VALUE
.PP
.RS 4
This function returns 1, 0, or \-1 if
.ft B
mt1
.ft R
is found greater than, equal to, or less than
.ft B
mt2\fP,
.ft R
respectively.
.RE
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBmtext_cmp()\fP,
.ft R
.ft B
mtext_casecmp()\fP,
.ft R
.ft B
mtext_ncasecmp()
.ft R
.ft B
mtext_compare()\fP,
.ft R
.ft B
mtext_case_compare()
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
