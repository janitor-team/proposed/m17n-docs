.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "FLT API" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
FLT_API \- API provided by libm17n\-flt.so.  

.SS "Data Structures"

.in +1c
.ti -1c
.RI "struct \fBMFLTGlyph\fP"
.br
.RI "\fIType of information about a glyph. \fP"
.ti -1c
.RI "struct \fBMFLTGlyphAdjustment\fP"
.br
.RI "\fIType of information about a glyph position adjustment. \fP"
.ti -1c
.RI "struct \fBMFLTGlyphString\fP"
.br
.RI "\fIType of information about a glyph sequence. \fP"
.ti -1c
.RI "struct \fBMFLTOtfSpec\fP"
.br
.RI "\fIType of specification of GSUB and GPOS OpenType tables. \fP"
.ti -1c
.RI "struct \fBMFLTFont\fP"
.br
.RI "\fIType of font to be used by the FLT driver. \fP"
.in -1c
.SS "Typedefs"

.in +1c
.ti -1c
.RI "typedef struct _MFLT \fBMFLT\fP"
.br
.RI "\fIType of FLT (Font Layout Table). \fP"
.in -1c
.SS "Functions"

.in +1c
.ti -1c
.RI "\fBMFLT\fP * \fBmflt_get\fP (\fBMSymbol\fP name)"
.br
.RI "\fIReturn an FLT object that has a specified name. \fP"
.ti -1c
.RI "\fBMFLT\fP * \fBmflt_find\fP (int c, \fBMFLTFont\fP *font)"
.br
.RI "\fIFind an FLT suitable for the specified character and font. \fP"
.ti -1c
.RI "const char * \fBmflt_name\fP (\fBMFLT\fP *flt)"
.br
.RI "\fIReturn the name of an FLT. \fP"
.ti -1c
.RI "\fBMCharTable\fP * \fBmflt_coverage\fP (\fBMFLT\fP *flt)"
.br
.RI "\fIReturn a coverage of a FLT. \fP"
.ti -1c
.RI "int \fBmflt_run\fP (\fBMFLTGlyphString\fP *gstring, int from, int to, \fBMFLTFont\fP *font, \fBMFLT\fP *flt)"
.br
.RI "\fILayout characters with an FLT. \fP"
.ti -1c
.RI "\fBMFLT\fP * \fBmdebug_dump_flt\fP (\fBMFLT\fP *flt, int indent)"
.br
.RI "\fIDump a Font Layout Table. \fP"
.ti -1c
.RI "void \fBmflt_dump_gstring\fP (\fBMFLTGlyphString\fP *gstring)"
.br
.RI "\fIDump an \fBMFLTGlyphString\fP. \fP"
.in -1c
.SS "Variables"

.in +1c
.ti -1c
.RI "int \fBmflt_enable_new_feature\fP"
.br
.RI "\fIFlag to control several new OTF handling commands. \fP"
.ti -1c
.RI "int(* \fBmflt_iterate_otf_feature\fP )(struct _MFLTFont *font, \fBMFLTOtfSpec\fP *spec, int from, int to, unsigned char *table)"
.br
.ti -1c
.RI "\fBMSymbol\fP(* \fBmflt_font_id\fP )(struct _MFLTFont *font)"
.br
.ti -1c
.RI "int(* \fBmflt_try_otf\fP )(struct _MFLTFont *font, \fBMFLTOtfSpec\fP *spec, \fBMFLTGlyphString\fP *gstring, int from, int to)"
.br
.in -1c
.SH "Detailed Description"
.PP 
API provided by libm17n\-flt.so. 

FLT support for a window system.
.PP
This section defines the m17n FLT API concerning character layouting facility using FLT (Font Layout Table). The format of FLT is described in \fBmdbFLT\fP. 
.SH "Data Structure Documentation"
.SS MFLTGlyph
.PP
.ad l
.nh
.PP
\fBFIELD DOCUMENTATION:\fP
.PP 
.PP
\fBint \fBMFLTGlyph::c\fP\fP
.PP
\fBunsigned int \fBMFLTGlyph::code\fP\fP
.PP
\fBint \fBMFLTGlyph::from\fP\fP
.PP
\fBint \fBMFLTGlyph::to\fP\fP
.PP
\fBint \fBMFLTGlyph::xadv\fP\fP
.PP
\fBint \fBMFLTGlyph::yadv\fP\fP
.PP
\fBint \fBMFLTGlyph::ascent\fP\fP
.PP
\fBint \fBMFLTGlyph::descent\fP\fP
.PP
\fBint \fBMFLTGlyph::lbearing\fP\fP
.PP
\fBint \fBMFLTGlyph::rbearing\fP\fP
.PP
\fBint \fBMFLTGlyph::xoff\fP\fP
.PP
\fBint \fBMFLTGlyph::yoff\fP\fP
.PP
\fBunsigned \fBMFLTGlyph::encoded\fP\fP
.PP
\fBunsigned \fBMFLTGlyph::measured\fP\fP
.PP
\fBunsigned \fBMFLTGlyph::adjusted\fP\fP
.PP
\fBunsigned \fBMFLTGlyph::internal\fP\fP

.SS MFLTGlyphAdjustment
.PP
.ad l
.nh
.PP
\fBFIELD DOCUMENTATION:\fP
.PP 
.PP
\fBint \fBMFLTGlyphAdjustment::xadv\fP\fP
.PP
\fBint \fBMFLTGlyphAdjustment::yadv\fP\fP
.PP
\fBint \fBMFLTGlyphAdjustment::xoff\fP\fP
.PP
\fBint \fBMFLTGlyphAdjustment::yoff\fP\fP
.PP
\fBshort \fBMFLTGlyphAdjustment::back\fP\fP
.PP
\fBunsigned \fBMFLTGlyphAdjustment::advance_is_absolute\fP\fP
.PP
\fBunsigned \fBMFLTGlyphAdjustment::set\fP\fP

.SS MFLTGlyphString
.PP
.ad l
.nh
.PP
\fBFIELD DOCUMENTATION:\fP
.PP 
.PP
\fBint \fBMFLTGlyphString::glyph_size\fP\fP
      (MFLTGlyph)'. 
.PP
\fB\fBMFLTGlyph\fP* \fBMFLTGlyphString::glyphs\fP\fP
.PP
\fBint \fBMFLTGlyphString::allocated\fP\fP
.PP
\fBint \fBMFLTGlyphString::used\fP\fP
.PP
\fBunsigned int \fBMFLTGlyphString::r2l\fP\fP

.SS MFLTOtfSpec
.PP
.ad l
.nh
.PP
\fBFIELD DOCUMENTATION:\fP
.PP 
.PP
\fB\fBMSymbol\fP \fBMFLTOtfSpec::sym\fP\fP
.PP
\fBunsigned int \fBMFLTOtfSpec::script\fP\fP
.PP
\fBunsigned int \fBMFLTOtfSpec::langsys\fP\fP
.PP
\fBunsigned int* \fBMFLTOtfSpec::features\fP[2]\fP
.PP
(1) The case of using this information for selecting which features to apply to a glyph string. If the array is NULL, apply no feature. If the first element is 0xFFFFFFFF, apply all available features except for what appear in the second and following elements (if any). Otherwise, apply all listed features.
.PP
(2) The case of using this information for checking if a a font can be drived by a specific FLT. If the array is NULL, the font should not have any features. Otherwize, the font should have all features before 0xFFFFFFFF element (if any) and should not have any features after that element. 

.SS MFLTFont
.PP
.ad l
.nh
.PP
\fBFIELD DOCUMENTATION:\fP
.PP 
.PP
\fB\fBMSymbol\fP \fBMFLTFont::family\fP\fP
.PP
\fBint \fBMFLTFont::x_ppem\fP\fP
.PP
\fBint \fBMFLTFont::y_ppem\fP\fP
.PP
\fBint(* \fBMFLTFont::get_glyph_id\fP)(struct _MFLTFont *font, \fBMFLTGlyphString\fP *gstring, int from, int to)\fP
.PP
\fBint(* \fBMFLTFont::get_metrics\fP)(struct _MFLTFont *font, \fBMFLTGlyphString\fP *gstring, int from, int to)\fP
.PP
\fBint(* \fBMFLTFont::check_otf\fP)(struct _MFLTFont *font, \fBMFLTOtfSpec\fP *spec)\fP
.PP
\fBint(* \fBMFLTFont::drive_otf\fP)(struct _MFLTFont *font, \fBMFLTOtfSpec\fP *spec, \fBMFLTGlyphString\fP *in, int from, int to, \fBMFLTGlyphString\fP *out, \fBMFLTGlyphAdjustment\fP *adjustment)\fP
.PP
\fBvoid* \fBMFLTFont::internal\fP\fP

.SH "Typedef Documentation"
.PP 
.SS "typedef struct _MFLT \fBMFLT\fP"
.PP
Type of FLT (Font Layout Table). The type \fBMFLT\fP is for an FLT object. Its internal structure is concealed from application programs. 
.SH "Variable Documentation"
.PP 
.SS "int \fBmflt_enable_new_feature\fP"
.PP
Flag to control several new OTF handling commands. If the variable mflt_enable_new_feature is nonzero, the function \fBmflt_run()\fP can drive a Font Layout Table that contains the new OTF\-related commands ':otf?' and/or OTF feature specification in a category table. 
.SS "int(* \fBmflt_iterate_otf_feature\fP)(struct _MFLTFont *font, \fBMFLTOtfSpec\fP *spec, int from, int to, unsigned char *table)"
.SS "\fBMSymbol\fP(* \fBmflt_font_id\fP)(struct _MFLTFont *font)"
.SS "int(* \fBmflt_try_otf\fP)(struct _MFLTFont *font, \fBMFLTOtfSpec\fP *spec, \fBMFLTGlyphString\fP *gstring, int from, int to)"
.SH "Author"
.PP 
Generated automatically by Doxygen for The m17n Library from the source code.
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
