.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mfontset_copy" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mfontset_copy \- Make a copy of a fontset. 


.SH SYNOPSIS
\fBMFontset\fP *
\fBmfontset_copy\fP (\fBMFontset\fP *
\fIfontset\fP, char *
\fIname\fP)


.SH DESCRIPTION
Make a copy of a fontset. The
.ft B
mfontset_copy()
.ft R
function makes a copy of fontset
.ft B
fontset\fP,
.ft R
gives it a name
.ft B
name\fP,
.ft R
and returns a pointer to the created copy.
.ft B
name
.ft R
must not be a name of existing fontset. In such case, this function returns NULL without making a copy. 
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
