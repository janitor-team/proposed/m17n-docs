.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mtext_search" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mtext_search \- Locate an M\-text in a specific range of another. 


.SH SYNOPSIS
int
\fBmtext_search\fP (\fBMText\fP *
\fImt1\fP, int
\fIfrom\fP, int
\fIto\fP, \fBMText\fP *
\fImt2\fP)


.SH DESCRIPTION
Locate an M\-text in a specific range of another. The
.ft B
mtext_search()
.ft R
function searches for the first occurrence of M\-text
.ft B
mt2
.ft R
in M\-text
.ft B
mt1
.ft R
in the region
.ft B
from
.ft R
and
.ft B
to
.ft R
while ignoring difference of the text properties. If
.ft B
from
.ft R
is less than
.ft B
to\fP,
.ft R
the forward search starts from
.ft B
from\fP,
.ft R
otherwise the backward search starts from
.ft B
to\fP.
.ft R
.PP
.SH RETURN VALUE
.PP
.RS 4
If
.ft B
mt2
.ft R
is found in
.ft B
mt1\fP,
.ft R
.ft B
mtext_search()
.ft R
returns the position of the first occurrence. Otherwise it returns \-1. If
.ft B
mt2
.ft R
is empty, it returns 0. 
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
