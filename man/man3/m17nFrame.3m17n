.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "Frame" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
Frame \- A \fIframe\fP is an object corresponding to the graphic device.  

.SS "Typedefs"

.in +1c
.ti -1c
.RI "typedef struct \fBMFrame\fP \fBMFrame\fP"
.br
.RI "\fIType of frames. \fP"
.in -1c
.SS "Functions"

.in +1c
.ti -1c
.RI "\fBMFrame\fP * \fBmframe\fP (\fBMPlist\fP *plist)"
.br
.RI "\fICreate a new frame. \fP"
.ti -1c
.RI "void * \fBmframe_get_prop\fP (\fBMFrame\fP *frame, \fBMSymbol\fP key)"
.br
.RI "\fIReturn property value of frame. \fP"
.in -1c
.SS "Variables"

.in +1c
.ti -1c
.RI "\fBMFrame\fP * \fBmframe_default\fP"
.br
.RI "\fIThe default frame. \fP"
.in -1c
.SS "Variables: Keys of frame parameter"
 These are the symbols to use in a parameter to create a frame. See the function \fBmframe()\fP for details.
.PP
\fBMdevice\fP, \fBMdisplay\fP, \fBMscreen\fP, \fBMdrawable\fP, \fBMdepth\fP, and \fBMcolormap\fP are also keys of a frame property. 
.in +1c
.ti -1c
.RI "\fBMSymbol\fP \fBMdevice\fP"
.br
.ti -1c
.RI "\fBMSymbol\fP \fBMdisplay\fP"
.br
.ti -1c
.RI "\fBMSymbol\fP \fBMscreen\fP"
.br
.ti -1c
.RI "\fBMSymbol\fP \fBMdrawable\fP"
.br
.ti -1c
.RI "\fBMSymbol\fP \fBMdepth\fP"
.br
.ti -1c
.RI "\fBMSymbol\fP \fBMcolormap\fP"
.br
.ti -1c
.RI "\fBMSymbol\fP \fBMwidget\fP"
.br
.ti -1c
.RI "\fBMSymbol\fP \fBMgd\fP"
.br
.in -1c
.SS "Variables: Keys of frame property"
 These are the symbols to use as an argument to the function \fBmframe_get_prop()\fP. 
.in +1c
.ti -1c
.RI "\fBMSymbol\fP \fBMfont\fP"
.br
.ti -1c
.RI "\fBMSymbol\fP \fBMfont_width\fP"
.br
.ti -1c
.RI "\fBMSymbol\fP \fBMfont_ascent\fP"
.br
.ti -1c
.RI "\fBMSymbol\fP \fBMfont_descent\fP"
.br
.in -1c
.SH "Detailed Description"
.PP 
A \fIframe\fP is an object corresponding to the graphic device. 

A \fIframe\fP is an object of the type \fBMFrame\fP to hold various information about each display/input device. Almost all m17n GUI functions require a pointer to a frame as an argument. 
.SH "Typedef Documentation"
.PP 
.SS "typedef struct \fBMFrame\fP \fBMFrame\fP"
.PP
Type of frames. The type \fBMFrame\fP is for a \fIframe\fP object. Each frame holds various information about the corresponding physical display/input device.
.PP
The internal structure of the type \fBMFrame\fP is concealed from an application program, and its contents depend on the window system in use. In the m17n\-X library, it contains the information about \fIdisplay\fP and \fIscreen\fP in the X Window System. 
.SH "Variable Documentation"
.PP 
.SS "\fBMSymbol\fP \fBMdevice\fP"
.SS "\fBMSymbol\fP \fBMdisplay\fP"
.SS "\fBMSymbol\fP \fBMscreen\fP"
.SS "\fBMSymbol\fP \fBMdrawable\fP"
.SS "\fBMSymbol\fP \fBMdepth\fP"
.SS "\fBMSymbol\fP \fBMcolormap\fP"
.SS "\fBMSymbol\fP \fBMwidget\fP"
.SS "\fBMSymbol\fP \fBMgd\fP"
.SS "\fBMSymbol\fP \fBMfont\fP"
.SS "\fBMSymbol\fP \fBMfont_width\fP"
.SS "\fBMSymbol\fP \fBMfont_ascent\fP"
.SS "\fBMSymbol\fP \fBMfont_descent\fP"
.SS "\fBMFrame\fP* \fBmframe_default\fP"
.PP
The default frame. The external variable \fBmframe_default\fP contains a pointer to the default frame that is created by the first call of \fBmframe()\fP. 
.SH "Author"
.PP 
Generated automatically by Doxygen for The m17n Library from the source code.
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
