.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mlocale_set" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mlocale_set \- Set the current locale. 


.SH SYNOPSIS
\fBMLocale\fP*
\fBmlocale_set\fP (int
\fIcategory\fP, const char *
\fIname\fP)


.SH DESCRIPTION
Set the current locale. The
.ft B
mlocale_set()
.ft R
function sets or query a part of the current locale. The part is specified by
.ft B
category
.ft R
which must be a valid first argument to
.ft C
setlocale()\fP.
.ft R
.PP
If
.ft B
locale
.ft R
is not NULL, the locale of the specified part is set to
.ft B
locale\fP.
.ft R
If
.ft B
locale
.ft R
is not supported by the system, the current locale is not changed.
.PP
If
.ft B
locale
.ft R
is NULL, the current locale of the specified part is queried.
.PP
.SH RETURN VALUE
.PP
.RS 4
If the call is successful,
.ft B
mlocale_set()
.ft R
returns an opaque locale object that corresponds to the locale. The name of the locale can be acquired by the function
.ft B
mlocale_get_prop()\fP.
.ft R
Otherwise, it returns NULL.
.RE
.PP
.SH ERRORS
.PP
.RS 4
\fCMERROR_LOCALE
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
