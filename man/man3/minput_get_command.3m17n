.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "minput_get_command" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
minput_get_command \- Get information about input method command(s). 


.SH SYNOPSIS
\fBMPlist\fP*
\fBminput_get_command\fP (\fBMSymbol\fP
\fIlanguage\fP, \fBMSymbol\fP
\fIname\fP, \fBMSymbol\fP
\fIcommand\fP)


.SH DESCRIPTION
Get information about input method command(s). The
.ft B
minput_get_command()
.ft R
function returns information about the command
.ft B
command
.ft R
of the input method specified by
.ft B
language
.ft R
and
.ft B
name\fP.
.ft R
An input method command is a pseudo key event to which one or more actual input key sequences are assigned.
.PP
There are two kinds of commands, global and local. A global command has a global definition, and the description and the key assignment may be inherited by a local command. Each input method defines a local command which has a local key assignment. It may also declare a local command that inherits the definition of a global command of the same name.
.PP
If
.ft B
language
.ft R
is
.ft B
Mt
.ft R
and
.ft B
name
.ft R
is
.ft B
Mnil\fP,
.ft R
this function returns information about a global command. Otherwise information about a local command is returned.
.PP
If
.ft B
command
.ft R
is
.ft B
Mnil\fP,
.ft R
information about all commands is returned.
.PP
The return value is a
.ft I
well\-formed
.ft R
plist (\fBProperty List\fP) of this format: 
.PP
.nf

  ((NAME DESCRIPTION STATUS [KEYSEQ ...]) ...)

.fi
.fi

.ft C
NAME
.ft R
is a symbol representing the command name.
.PP
\fCDESCRIPTION
.ft R
is an M\-text describing the command, or
.ft B
Mnil
.ft R
if the command has no description.
.PP
\fCSTATUS
.ft R
is a symbol representing how the key assignment is decided. The value is
.ft B
Mnil
.ft R
(the default key assignment),
.ft B
Mcustomized
.ft R
(the key assignment is customized by per\-user customization file), or
.ft B
Mconfigured
.ft R
(the key assignment is set by the call of
.ft B
minput_config_command()\fP). For a local command only, it may also be
.ft B
Minherited
.ft R
(the key assignment is inherited from the corresponding global command).
.PP
\fCKEYSEQ
.ft R
is a plist of one or more symbols representing a key sequence assigned to the command. If there's no KEYSEQ, the command is currently disabled (i.e. no key sequence can trigger actions of the command).
.PP
If
.ft B
command
.ft R
is not
.ft B
Mnil\fP,
.ft R
the first element of the returned plist contains the information about
.ft B
command\fP.
.ft R
.PP
.SH RETURN VALUE
.PP
.RS 4

.RE
.PP
If the requested information was found, a pointer to a non\-empty plist is returned. As the plist is kept in the library, the caller must not modify nor free it.
.PP
Otherwise (the specified input method or the specified command does not exist),
.ft C
NULL
.ft R
is returned.
.PP
.SH Example:
.PP

.RS 4

.PP
.nf
MText *
get_im_command_description (MSymbol language, MSymbol name, MSymbol command)
{
  /* Return a description of the command COMMAND of the input method
     specified by LANGUAGE and NAME.  */
  MPlist *cmd = minput_get_command (langauge, name, command);
  MPlist *plist;

  if (! cmds)
    return NULL;
  plist = mplist_value (cmds);  /* (NAME DESCRIPTION STATUS KEY\-SEQ ...) */
  plist = mplist_next (plist);  /* (DESCRIPTION STATUS KEY\-SEQ ...) */
  return  (mplist_key (plist) == Mtext
           ? (MText *) mplist_value (plist)
           : NULL);
}

.fi
.fi
 
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
