.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "m17n_status" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
m17n_status \- Report which part of the m17n library is initialized. 


.SH SYNOPSIS
enum \fBM17NStatus\fP
\fBm17n_status\fP (void)


.SH DESCRIPTION
Report which part of the m17n library is initialized. The
.ft B
m17n_status()
.ft R
function returns one of these values depending on which part of the m17n library is initialized:
.PP
\fBM17N_NOT_INITIALIZED\fP,
.ft R
.ft B
M17N_CORE_INITIALIZED\fP,
.ft R
.ft B
M17N_SHELL_INITIALIZED\fP,
.ft R
.ft B
M17N_GUI_INITIALIZED
.ft R
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
