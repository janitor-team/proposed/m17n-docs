.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mfont_unparse_name" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mfont_unparse_name \- Create a fontname from a font. 


.SH SYNOPSIS
char*
\fBmfont_unparse_name\fP (\fBMFont\fP *
\fIfont\fP, \fBMSymbol\fP
\fIformat\fP)


.SH DESCRIPTION
Create a fontname from a font. The
.ft B
mfont_unparse_name()
.ft R
function creates a fontname string from font
.ft B
font
.ft R
according to
.ft B
format\fP.
.ft R
.PP
\fBformat
.ft R
must be
.ft B
Mx
.ft R
or
.ft B
Mfontconfig\fP.
.ft R
If it is
.ft B
Mx\fP,
.ft R
the fontname is in XLFD (X Logical Font Description) format. If it is
.ft B
Mfontconfig\fP,
.ft R
the fontname is in the style of Fontconfig's text representation.
.PP
.SH RETURN VALUE
.PP
.RS 4
This function returns a newly allocated fontname string, which is not freed unless the user explicitly does so by free(). 
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
