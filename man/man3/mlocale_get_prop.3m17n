.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mlocale_get_prop" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mlocale_get_prop \- Get the value of a locale property. 


.SH SYNOPSIS
\fBMSymbol\fP
\fBmlocale_get_prop\fP (\fBMLocale\fP *
\fIlocale\fP, \fBMSymbol\fP
\fIkey\fP)


.SH DESCRIPTION
Get the value of a locale property. The
.ft B
mlocale_get_prop()
.ft R
function returns the value of a property
.ft B
key
.ft R
of local
.ft B
locale\fP.
.ft R
.ft B
key
.ft R
must be
.ft B
Mname\fP,
.ft R
.ft B
Mlanguage\fP,
.ft R
.ft B
Mterritory\fP,
.ft R
.ft B
Mcodeset\fP,
.ft R
.ft B
Mmodifier\fP,
.ft R
or
.ft B
Mcoding\fP.
.ft R
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
