.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mdraw_per_char_extents" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mdraw_per_char_extents \- Obtain per character dimension information. 


.SH SYNOPSIS
void
\fBmdraw_per_char_extents\fP (\fBMFrame\fP *
\fIframe\fP, \fBMText\fP *
\fImt\fP, \fBMDrawMetric\fP *
\fIarray_return\fP, \fBMDrawMetric\fP *
\fIoverall_return\fP)


.SH DESCRIPTION
Obtain per character dimension information. The
.ft B
mdraw_per_char_extents()
.ft R
function computes the text dimension of each character in M\-text
.ft B
mt\fP.
.ft R
The faces given as text properties in
.ft B
mt
.ft R
and the default face of frame
.ft B
frame
.ft R
determine the fonts to draw the text. Each successive element in
.ft B
array_return
.ft R
is set to the drawn metrics of successive characters, which is relative to the origin of the drawing, and a rectangle for each character in
.ft B
mt\fP.
.ft R
The number of elements of
.ft B
array_return
.ft R
must be equal to or greater than the number of characters in
.ft B
mt\fP.
.ft R
.PP
If pointer
.ft B
overall_return
.ft R
is not
.ft C
NULL\fP,
.ft R
this function also computes the extents of the overall text and stores the results in the members of the structure pointed to by
.ft B
overall_return\fP.
.ft R
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
