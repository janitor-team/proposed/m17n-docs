.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mfontset_modify_entry" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mfontset_modify_entry \- Modify the contents of a fontset. 


.SH SYNOPSIS
int
\fBmfontset_modify_entry\fP (\fBMFontset\fP *
\fIfontset\fP, \fBMSymbol\fP
\fIscript\fP, \fBMSymbol\fP
\fIlanguage\fP, \fBMSymbol\fP
\fIcharset\fP, \fBMFont\fP *
\fIspec\fP, \fBMSymbol\fP
\fIlayouter_name\fP, int
\fIhow\fP)


.SH DESCRIPTION
Modify the contents of a fontset. The
.ft B
mfontset_modify_entry()
.ft R
function associates, in fontset
.ft B
fontset\fP,
.ft R
a copy of
.ft B
font
.ft R
with the
.ft B
script
.ft R
/
.ft B
language
.ft R
pair or with
.ft B
charset\fP.
.ft R
.PP
Each font in a fontset is associated with a particular script/language pair, with a particular charset, or with the symbol
.ft C
Mnil\fP.
.ft R
The fonts that are associated with the same item make a group.
.PP
If
.ft B
script
.ft R
is not
.ft C
Mnil\fP,
.ft R
it must be a symbol identifying a script. In this case,
.ft B
language
.ft R
is either a symbol identifying a language or
.ft C
Mnil\fP,
.ft R
and
.ft B
font
.ft R
is associated with the
.ft B
script
.ft R
/
.ft B
language
.ft R
pair.
.PP
If
.ft B
charset
.ft R
is not
.ft C
Mnil\fP,
.ft R
it must be a symbol representing a charset object. In this case,
.ft B
font
.ft R
is associated with that charset.
.PP
If both
.ft B
script
.ft R
and
.ft B
charset
.ft R
are not
.ft C
Mnil\fP,
.ft R
two copies of
.ft B
font
.ft R
are created. Then one is associated with the
.ft B
script
.ft R
/
.ft B
language
.ft R
pair and the other with that charset.
.PP
If both
.ft B
script
.ft R
and
.ft B
charset
.ft R
are
.ft C
Mnil\fP,
.ft R
.ft B
font
.ft R
is associated with
.ft C
Mnil\fP.
.ft R
This kind of fonts are called
.ft I
fallback
.ft R
.ft I
fonts\fP.
.ft R
.PP
The argument
.ft B
how
.ft R
specifies the priority of
.ft B
font\fP.
.ft R
If
.ft B
how
.ft R
is positive,
.ft B
font
.ft R
has the highest priority in the group of fonts that are associated with the same item. If
.ft B
how
.ft R
is negative,
.ft B
font
.ft R
has the lowest priority. If
.ft B
how
.ft R
is zero,
.ft B
font
.ft R
becomes the only available font for the associated item; all the other fonts are removed from the group.
.PP
If
.ft B
layouter_name
.ft R
is not
.ft C
Mnil\fP,
.ft R
it must be a symbol representing a
.ft B
mdbFLT
.ft R
(font layout table). In that case, if
.ft B
font
.ft R
is selected for drawing an M\-text, that font layout table is used to generate a glyph code sequence from a character sequence.
.PP
.SH RETURN VALUE
.PP
.RS 4
If the operation was successful,
.ft B
mfontset_modify_entry()
.ft R
returns 0. Otherwise it returns \-1 and assigns an error code to the external variable
.ft B
merror_code\fP.
.ft R
.RE
.PP
.SH ERRORS
.PP
.RS 4
\fCMERROR_SYMBOL
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
