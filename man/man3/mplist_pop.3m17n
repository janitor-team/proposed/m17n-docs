.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mplist_pop" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mplist_pop \- Remove a property at the beginning of a property list. 


.SH SYNOPSIS
void*
\fBmplist_pop\fP (\fBMPlist\fP *
\fIplist\fP)


.SH DESCRIPTION
Remove a property at the beginning of a property list. The
.ft B
mplist_pop()
.ft R
function removes a property at the beginning of property list
.ft B
plist\fP.
.ft R
As a result, the second key and value of the
.ft B
plist
.ft R
become the first ones.
.PP
.SH RETURN VALUE
.PP
.RS 4
If the operation was successful, this function return the value of the just popped property. Otherwise, it returns
.ft C
NULL\fP.
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
