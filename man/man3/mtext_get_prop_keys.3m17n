.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mtext_get_prop_keys" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mtext_get_prop_keys \- Get a list of text property keys at a position of an M\-text. 


.SH SYNOPSIS
int
\fBmtext_get_prop_keys\fP (\fBMText\fP *
\fImt\fP, int
\fIpos\fP, \fBMSymbol\fP **
\fIkeys\fP)


.SH DESCRIPTION
Get a list of text property keys at a position of an M\-text. The
.ft B
mtext_get_prop_keys()
.ft R
function creates an array whose elements are the keys of text properties found at position
.ft B
pos
.ft R
in M\-text
.ft B
mt\fP,
.ft R
and sets *\fBkeys
.ft R
to the address of the created array. The user is responsible to free the memory allocated for the array.
.PP
.SH RETURN VALUE
.PP
.RS 4
If the operation was successful,
.ft B
mtext_get_prop_keys()
.ft R
returns the length of the key list. Otherwise it returns \-1 and assigns an error code to the external variable
.ft B
merror_code\fP.
.ft R
.RE
.PP
.SH ERRORS
.PP
.RS 4
\fCMERROR_RANGE
.ft R
.RE
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBmtext_get_prop()\fP,
.ft R
.ft B
mtext_put_prop()\fP,
.ft R
.ft B
mtext_put_prop_values()\fP,
.ft R
.ft B
mtext_get_prop_values()\fP,
.ft R
.ft B
mtext_push_prop()\fP,
.ft R
.ft B
mtext_pop_prop()
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
