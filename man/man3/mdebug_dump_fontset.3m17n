.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mdebug_dump_fontset" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mdebug_dump_fontset \- Dump a fontset. 


.SH SYNOPSIS
\fBMFontset\fP*
\fBmdebug_dump_fontset\fP (\fBMFontset\fP *
\fIfontset\fP, int
\fIindent\fP)


.SH DESCRIPTION
Dump a fontset. The
.ft B
mdebug_dump_fontset()
.ft R
function prints fontset
.ft B
fontset
.ft R
in a human readable way to the stderr or to what specified by the environment variable MDEBUG_OUTPUT_FILE.
.ft B
indent
.ft R
specifies how many columns to indent the lines but the first one.
.PP
.SH RETURN VALUE
.PP
.RS 4
This function returns
.ft B
fontset\fP.
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
