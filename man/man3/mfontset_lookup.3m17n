.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mfontset_lookup" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mfontset_lookup \- Lookup a fontset. 


.SH SYNOPSIS
\fBMPlist\fP *
\fBmfontset_lookup\fP (\fBMFontset\fP *
\fIfontset\fP, \fBMSymbol\fP
\fIscript\fP, \fBMSymbol\fP
\fIlanguage\fP, \fBMSymbol\fP
\fIcharset\fP)


.SH DESCRIPTION
Lookup a fontset. The
.ft B
mfontset_lookup()
.ft R
function lookups
.ft B
fontset
.ft R
and returns a plist that describes the contents of
.ft B
fontset
.ft R
corresponding to the specified script, language, and charset.
.PP
If
.ft B
script
.ft R
is
.ft C
Mt\fP,
.ft R
keys of the returned plist are script name symbols for which some fonts are specified and values are NULL.
.PP
If
.ft B
script
.ft R
is a script name symbol, the returned plist is decided by
.ft B
language\fP.
.ft R
.PP
.PD 0
.IP "\(bu" 2
If
.ft B
language
.ft R
is
.ft C
Mt\fP,
.ft R
keys of the plist are language name symbols for which some fonts are specified and values are NULL. A key may be
.ft C
Mt
.ft R
which means some fallback fonts are specified for the script.
.PP
.PD 0
.IP "\(bu" 2
If
.ft B
language
.ft R
is a language name symbol, the plist is a
.ft C
FONT\-GROUP
.ft R
for the specified script and language.
.ft C
FONT\-GROUP
.ft R
is a plist whose keys are FLT (FontLayoutTable) name symbols (\fCMt
.ft R
if no FLT is associated with the font) and values are pointers to
.ft B
MFont\fP.
.ft R
.PP
.PD 0
.IP "\(bu" 2
If
.ft B
language
.ft R
is
.ft C
Mnil\fP,
.ft R
the plist is fallback
.ft C
FONT\-GROUP
.ft R
for the script.
.PP
If
.ft B
script
.ft R
is
.ft C
Mnil\fP,
.ft R
the returned plist is decided as below.
.PP
.PD 0
.IP "\(bu" 2
If
.ft B
charset
.ft R
is
.ft C
Mt\fP,
.ft R
keys of the returned plist are charset name symbols for which some fonts are specified and values are NULL.
.PP
.PD 0
.IP "\(bu" 2
If
.ft B
charset
.ft R
is a charset name symbol, the plist is a
.ft C
FONT\-GROUP
.ft R
for the charset.
.PP
.PD 0
.IP "\(bu" 2
If
.ft B
charset
.ft R
is
.ft C
Mnil\fP,
.ft R
the plist is a fallback
.ft C
FONT\-GROUP\fP.
.ft R
.PP
.SH RETURN VALUE
.PP
.RS 4
It returns a plist describing the contents of a fontset. The plist should be freed by
.ft B
m17n_object_unref()\fP.
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
