.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "msymbol_get_func" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
msymbol_get_func \- Get the value (function pointer) of a symbol property. 


.SH SYNOPSIS
\fBM17NFunc\fP
\fBmsymbol_get_func\fP (\fBMSymbol\fP
\fIsymbol\fP, \fBMSymbol\fP
\fIkey\fP)


.SH DESCRIPTION
Get the value (function pointer) of a symbol property. The
.ft B
msymbol_get_func()
.ft R
function is similar to
.ft B
msymbol_get()
.ft R
but for getting a function pointer form the property of symbol
.ft B
symbol\fP.
.ft R
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBmsymbol_get()
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
