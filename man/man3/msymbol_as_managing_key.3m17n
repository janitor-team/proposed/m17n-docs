.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "msymbol_as_managing_key" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
msymbol_as_managing_key \- Create a managing key. 


.SH SYNOPSIS
\fBMSymbol\fP
\fBmsymbol_as_managing_key\fP (const char *
\fIname\fP)


.SH DESCRIPTION
Create a managing key. The
.ft B
msymbol_as_managing_key()
.ft R
function returns a newly created managing key whose name is
.ft B
name\fP.
.ft R
It there already exists a symbol of name
.ft B
name\fP,
.ft R
it returns
.ft B
Mnil\fP.
.ft R
.PP
Symbols whose name starts by two spaces are reserved by the m17n library, and are used by the library only internally.
.PP
.SH RETURN VALUE
.PP
.RS 4
If the operation was successful, this function returns the created symbol. Otherwise, it returns
.ft B
Mnil\fP.
.ft R
.RE
.PP
.SH ERRORS
.PP
.RS 4
MERROR_SYMBOL
.RE
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBmsymbol()\fP,
.ft R
.ft B
msymbol_exist()
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
