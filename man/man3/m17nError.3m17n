.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "Error Handling" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
Error_Handling \- Error handling of the m17n library.  

.SS "Enumerations"

.in +1c
.ti -1c
.RI "enum \fBMErrorCode\fP { \fBMERROR_NONE\fP, \fBMERROR_OBJECT\fP, \fBMERROR_SYMBOL\fP, \fBMERROR_MTEXT\fP, \fBMERROR_TEXTPROP\fP, \fBMERROR_CHAR\fP, \fBMERROR_CHARTABLE\fP, \fBMERROR_CHARSET\fP, \fBMERROR_CODING\fP, \fBMERROR_RANGE\fP, \fBMERROR_LANGUAGE\fP, \fBMERROR_LOCALE\fP, \fBMERROR_PLIST\fP, \fBMERROR_MISC\fP, \fBMERROR_WIN\fP, \fBMERROR_X\fP, \fBMERROR_FRAME\fP, \fBMERROR_FACE\fP, \fBMERROR_DRAW\fP, \fBMERROR_FLT\fP, \fBMERROR_FONT\fP, \fBMERROR_FONTSET\fP, \fBMERROR_FONT_OTF\fP, \fBMERROR_FONT_X\fP, \fBMERROR_FONT_FT\fP, \fBMERROR_IM\fP, \fBMERROR_DB\fP, \fBMERROR_IO\fP, \fBMERROR_DEBUG\fP, \fBMERROR_MEMORY\fP, \fBMERROR_GD\fP, \fBMERROR_MAX\fP }"
.br
.RI "\fIEnumeration for error code of the m17n library. \fP"
.in -1c
.SS "Variables"

.in +1c
.ti -1c
.RI "int \fBmerror_code\fP"
.br
.RI "\fIExternal variable to hold error code of the m17n library. \fP"
.ti -1c
.RI "void(* \fBm17n_memory_full_handler\fP )(enum \fBMErrorCode\fP err)"
.br
.RI "\fIMemory allocation error handler. \fP"
.in -1c
.SH "Detailed Description"
.PP 
Error handling of the m17n library. 

There are two types of errors that may happen in a function of the m17n library.
.PP
The first type is argument errors. When a library function is called with invalid arguments, it returns a value that indicates error and at the same time sets the external variable \fBmerror_code\fP to a non\-zero integer.
.PP
The second type is memory allocation errors. When the required amount of memory is not available on the system, m17n library functions call a function pointed to by the external variable \fCm17n_memory_full_handler\fP. The default value of the variable is a pointer to the default_error_handle() function, which just calls \fC exit()\fP. 
.SH "Enumeration Type Documentation"
.PP 
.SS "enum \fBMErrorCode\fP"
.PP
Enumeration for error code of the m17n library. Enumeration for error code of the m17n library.
.PP
When a library function is called with an invalid argument, it sets the external variable \fBmerror_code\fP to one of these values. All the error codes are positive integers.
.PP
When a memory allocation error happens, the function pointed to by the external variable \fBm17n_memory_full_handler\fP is called with one of these values as an argument. 
.PP
\fBEnumerator: \fP
.in +1c
.TP
\fB\fIMERROR_NONE \fP\fP
.TP
\fB\fIMERROR_OBJECT \fP\fP
.TP
\fB\fIMERROR_SYMBOL \fP\fP
.TP
\fB\fIMERROR_MTEXT \fP\fP
.TP
\fB\fIMERROR_TEXTPROP \fP\fP
.TP
\fB\fIMERROR_CHAR \fP\fP
.TP
\fB\fIMERROR_CHARTABLE \fP\fP
.TP
\fB\fIMERROR_CHARSET \fP\fP
.TP
\fB\fIMERROR_CODING \fP\fP
.TP
\fB\fIMERROR_RANGE \fP\fP
.TP
\fB\fIMERROR_LANGUAGE \fP\fP
.TP
\fB\fIMERROR_LOCALE \fP\fP
.TP
\fB\fIMERROR_PLIST \fP\fP
.TP
\fB\fIMERROR_MISC \fP\fP
.TP
\fB\fIMERROR_WIN \fP\fP
.TP
\fB\fIMERROR_X \fP\fP
.TP
\fB\fIMERROR_FRAME \fP\fP
.TP
\fB\fIMERROR_FACE \fP\fP
.TP
\fB\fIMERROR_DRAW \fP\fP
.TP
\fB\fIMERROR_FLT \fP\fP
.TP
\fB\fIMERROR_FONT \fP\fP
.TP
\fB\fIMERROR_FONTSET \fP\fP
.TP
\fB\fIMERROR_FONT_OTF \fP\fP
.TP
\fB\fIMERROR_FONT_X \fP\fP
.TP
\fB\fIMERROR_FONT_FT \fP\fP
.TP
\fB\fIMERROR_IM \fP\fP
.TP
\fB\fIMERROR_DB \fP\fP
.TP
\fB\fIMERROR_IO \fP\fP
.TP
\fB\fIMERROR_DEBUG \fP\fP
.TP
\fB\fIMERROR_MEMORY \fP\fP
.TP
\fB\fIMERROR_GD \fP\fP
.TP
\fB\fIMERROR_MAX \fP\fP

.SH "Variable Documentation"
.PP 
.SS "int \fBmerror_code\fP"
.PP
External variable to hold error code of the m17n library. The external variable \fBmerror_code\fP holds an error code of the m17n library. When a library function is called with an invalid argument, it sets this variable to one of \fCenum\fP \fBMErrorCode\fP.
.PP
This variable initially has the value 0. 
.SS "void(* \fBm17n_memory_full_handler\fP)(enum \fBMErrorCode\fP err)"
.PP
Memory allocation error handler. The external variable \fBm17n_memory_full_handler\fP holds a pointer to the function to call when a library function failed to allocate memory. \fBerr\fP is one of \fCenum\fP \fBMErrorCode\fP indicating in which function the error occurred.
.PP
This variable initially points a function that simply calls the \fCexit \fP() function with \fBerr\fP as an argument.
.PP
An application program that needs a different error handling can change this variable to point a proper function. 
.SH "Author"
.PP 
Generated automatically by Doxygen for The m17n Library from the source code.
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
