.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mfont_list" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mfont_list \- Get a list of fonts. 


.SH SYNOPSIS
\fBMPlist\fP*
\fBmfont_list\fP (\fBMFrame\fP *
\fIframe\fP, \fBMFont\fP *
\fIfont\fP, \fBMSymbol\fP
\fIlanguage\fP, int
\fImaxnum\fP)


.SH DESCRIPTION
Get a list of fonts. The
.ft B
mfont_list()
.ft R
functions returns a list of fonts available on frame
.ft B
frame\fP.
.ft R
.ft B
font\fP,
.ft R
if not NULL, limits fonts to ones that match with
.ft B
font\fP.
.ft R
.ft B
language\fP,
.ft R
if not
.ft C
Mnil\fP,
.ft R
limits fonts to ones that support
.ft B
language\fP.
.ft R
.ft B
maxnum\fP,
.ft R
if greater than 0, limits the number of fonts.
.PP
\fBlanguage
.ft R
argument exists just for backward compatibility, and the use is deprecated. Use
.ft B
Mlanguage
.ft R
font property instead. If
.ft B
font
.ft R
already has
.ft B
Mlanguage
.ft R
property,
.ft B
language
.ft R
is ignored.
.PP
.SH RETURN VALUE
.PP
.RS 4
This function returns a plist whose keys are family names and values are pointers to the object MFont. The plist must be freed by
.ft B
m17n_object_unref()\fP.
.ft R
If no font is found, it returns NULL. 
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
