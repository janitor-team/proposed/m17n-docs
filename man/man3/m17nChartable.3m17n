.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "Chartable" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
Chartable \- Chartable objects and API for them.  

.SS "Typedefs"

.in +1c
.ti -1c
.RI "typedef struct \fBMCharTable\fP \fBMCharTable\fP"
.br
.RI "\fIType of chartables. \fP"
.in -1c
.SS "Functions"

.in +1c
.ti -1c
.RI "\fBMCharTable\fP * \fBmchartable\fP (\fBMSymbol\fP key, void *default_value)"
.br
.RI "\fICreate a new chartable. \fP"
.ti -1c
.RI "int \fBmchartable_min_char\fP (\fBMCharTable\fP *table)"
.br
.RI "\fIReturn the minimum character whose value is set in a chartabe. \fP"
.ti -1c
.RI "int \fBmchartable_max_char\fP (\fBMCharTable\fP *table)"
.br
.RI "\fIReturn the maximum character whose value is set in a chartabe. \fP"
.ti -1c
.RI "void * \fBmchartable_lookup\fP (\fBMCharTable\fP *table, int c)"
.br
.RI "\fIReturn the assigned value of a character in a chartable. \fP"
.ti -1c
.RI "int \fBmchartable_set\fP (\fBMCharTable\fP *table, int c, void *val)"
.br
.RI "\fIAssign a value to a character in a chartable. \fP"
.ti -1c
.RI "int \fBmchartable_set_range\fP (\fBMCharTable\fP *table, int from, int to, void *val)"
.br
.RI "\fIAssign a value to the characters in the specified range. \fP"
.ti -1c
.RI "void \fBmchartable_range\fP (\fBMCharTable\fP *table, int *from, int *to)"
.br
.RI "\fISearch for characters that have non-default value. \fP"
.ti -1c
.RI "int \fBmchartable_map\fP (\fBMCharTable\fP *table, void *ignore, void(*func)(int, int, void *, void *), void *func_arg)"
.br
.RI "\fICall a function for characters in a chartable. \fP"
.in -1c
.SS "Variables"

.in +1c
.ti -1c
.RI "\fBMSymbol\fP \fBMchar_table\fP"
.br
.RI "\fISymbol whose name is 'char-table'. \fP"
.in -1c
.SH "Detailed Description"
.PP 
Chartable objects and API for them. 

The m17n library supports enormous number of characters. Thus, if attributes of each character are to be stored in a simple array, such an array would be impractically big. The attributes usually used, however, are often assigned only to a range of characters. Even when all characters have attributes, characters of consecutive character code tend to have the same attribute values.
.PP
The m17n library utilizes this tendency to store characters and their attribute values efficiently in an object called \fIChartable\fP. Although a chartable object is not a simple array, application programs can handle a chartable as if it is an array. Attribute values of a character can be obtained by accessing a Chartable for the attribute with the character code of the specified character.
.PP
A chartable is a managed object. 
.SH "Typedef Documentation"
.PP 
.SS "typedef struct \fBMCharTable\fP \fBMCharTable\fP"
.PP
Type of chartables. The type \fBMCharTable\fP is for a \fIchartable\fP objects. Its internal structure is concealed from application programs. 
.SH "Variable Documentation"
.PP 
.SS "\fBMSymbol\fP \fBMchar_table\fP"
.PP
Symbol whose name is 'char\-table'. The symbol \fCMchar_table\fP has the name \fC'char\-table'\fP. 
.SH "Author"
.PP 
Generated automatically by Doxygen for The m17n Library from the source code.
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
