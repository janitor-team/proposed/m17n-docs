.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mtext_pop_prop" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mtext_pop_prop \- Pop a text property. 


.SH SYNOPSIS
int
\fBmtext_pop_prop\fP (\fBMText\fP *
\fImt\fP, int
\fIfrom\fP, int
\fIto\fP, \fBMSymbol\fP
\fIkey\fP)


.SH DESCRIPTION
Pop a text property. The
.ft B
mtext_pop_prop()
.ft R
function removes the topmost text property whose key is
.ft B
key
.ft R
from the characters between
.ft B
from
.ft R
(inclusive) and and
.ft B
to
.ft R
(exclusive) in
.ft B
mt\fP.
.ft R
.PP
This function does nothing if characters in the region have no such text property. With this function,
.PP
.PP
.nf

                    FROM                    TO
M\-text: |<\-\-\-\-\-\-\-\-\-\-\-\-|\-\-\-\-\-\-\-\- MT \-\-\-\-\-\-\-\-\-|\-\-\-\-\-\-\-\-\-\-\-\->|
PROP  :  <\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\- OLD_VAL \-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\->
.fi
.fi
.PP
becomes
.PP
.PP
.nf

                    FROM                    TO
M\-text: |<\-\-\-\-\-\-\-\-\-\-\-\-|\-\-\-\-\-\-\-\- MT \-\-\-\-\-\-\-\-\-|\-\-\-\-\-\-\-\-\-\-\-\->|
PROP  :  <\-\-OLD_VAL\-\->|                     |<\-\-OLD_VAL\-\->|
.fi
.fi
.PP
.SH RETURN VALUE
.PP
.RS 4
If the operation was successful,
.ft B
mtext_pop_prop()
.ft R
return 0. Otherwise it returns \-1 and assigns an error code to the external variable
.ft B
merror_code\fP.
.ft R
.RE
.PP
.SH ERRORS
.PP
.RS 4
\fCMERROR_RANGE\fP,
.ft R
.ft C
MERROR_SYMBOL
.ft R
.RE
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBmtext_put_prop()\fP,
.ft R
.ft B
mtext_put_prop_values()\fP,
.ft R
.ft B
mtext_get_prop()\fP,
.ft R
.ft B
mtext_get_prop_values()\fP,
.ft R
.ft B
mtext_push_prop()\fP,
.ft R
.ft B
mtext_prop_range()
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
