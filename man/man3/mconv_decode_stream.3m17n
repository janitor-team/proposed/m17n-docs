.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mconv_decode_stream" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mconv_decode_stream \- Decode a stream input based on a coding system. 


.SH SYNOPSIS
\fBMText\fP*
\fBmconv_decode_stream\fP (\fBMSymbol\fP
\fIname\fP, FILE *
\fIfp\fP)


.SH DESCRIPTION
Decode a stream input based on a coding system. The
.ft B
mconv_decode_stream()
.ft R
function decodes the entire byte sequence read in from stream
.ft B
fp
.ft R
based on the coding system
.ft B
name\fP.
.ft R
A code converter for decoding is automatically created and freed.
.PP
.SH RETURN VALUE
.PP
.RS 4
If the operation was successful,
.ft B
mconv_decode_stream()
.ft R
returns the resulting M\-text. Otherwise it returns
.ft C
NULL
.ft R
and assigns an error code to the external variable
.ft B
merror_code\fP.
.ft R
.RE
.PP
.SH ERRORS
.PP
.RS 4
\fCMERROR_IO\fP,
.ft R
.ft C
MERROR_CODING
.ft R
.RE
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBmconv_decode()\fP,
.ft R
.ft B
mconv_decode_buffer()
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
