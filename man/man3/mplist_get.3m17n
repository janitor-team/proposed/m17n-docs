.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mplist_get" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mplist_get \- Get the value of a property in a property list. 


.SH SYNOPSIS
void*
\fBmplist_get\fP (\fBMPlist\fP *
\fIplist\fP, \fBMSymbol\fP
\fIkey\fP)


.SH DESCRIPTION
Get the value of a property in a property list. The
.ft B
mplist_get()
.ft R
function searches property list
.ft B
plist
.ft R
from the beginning for a property whose key is
.ft B
key\fP.
.ft R
If such a property is found, its value is returned as the type of
.ft C
(void *)\fP.
.ft R
If not found,
.ft C
NULL
.ft R
is returned.
.PP
When
.ft C
NULL
.ft R
is returned, there are two possibilities: one is the case where no property is found (see above); the other is the case where a property is found and its value is
.ft C
NULL\fP.
.ft R
In case that these two cases must be distinguished, use the
.ft B
mplist_find_by_key()
.ft R
function.
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBmplist_find_by_key()
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
