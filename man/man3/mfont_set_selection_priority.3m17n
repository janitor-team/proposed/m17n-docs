.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mfont_set_selection_priority" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mfont_set_selection_priority \- Set the font selection priority. 


.SH SYNOPSIS
int
\fBmfont_set_selection_priority\fP (\fBMSymbol\fP *
\fIkeys\fP)


.SH DESCRIPTION
Set the font selection priority. The
.ft B
mfont_set_selection_priority()
.ft R
function sets font selection priority according to
.ft B
keys\fP,
.ft R
which is an array of six symbols. Each element must be one of the below. No two elements must be the same.
.PP
\fCMfamily\fP,
.ft R
.ft C
Mweight\fP,
.ft R
.ft C
Mstyle\fP,
.ft R
.ft C
Mstretch\fP,
.ft R
.ft C
Madstyle\fP,
.ft R
.ft C
Msize\fP.
.ft R
.PP
See the documentation of the function
.ft B
mfont_selection_priority()
.ft R
for details. 
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
