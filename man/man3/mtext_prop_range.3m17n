.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mtext_prop_range" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mtext_prop_range \- Find the range where the value of a text property is the same. 


.SH SYNOPSIS
int
\fBmtext_prop_range\fP (\fBMText\fP *
\fImt\fP, \fBMSymbol\fP
\fIkey\fP, int
\fIpos\fP, int *
\fIfrom\fP, int *
\fIto\fP, int
\fIdeeper\fP)


.SH DESCRIPTION
Find the range where the value of a text property is the same. The
.ft B
mtext_prop_range()
.ft R
function investigates the extent where all characters have the same value for a text property. It first finds the value of the property specified by
.ft B
key
.ft R
of the character at
.ft B
pos
.ft R
in M\-text
.ft B
mt\fP.
.ft R
Then it checks if adjacent characters have the same value for the property
.ft B
key\fP.
.ft R
The beginning and the end of the found range are stored to the variable pointed to by
.ft B
from
.ft R
and
.ft B
to\fP.
.ft R
The character position stored in
.ft B
from
.ft R
is inclusive but that in
.ft B
to
.ft R
is exclusive; this fashion is compatible with the range specification in the
.ft B
mtext_put_prop()
.ft R
function, etc.
.PP
If
.ft B
deeper
.ft R
is not 0, not only the topmost but also all the stacked properties whose key is
.ft B
key
.ft R
are compared.
.PP
If
.ft B
from
.ft R
is
.ft C
NULL\fP,
.ft R
the beginning of range is not searched for. If
.ft B
to
.ft R
is
.ft C
NULL\fP,
.ft R
the end of range is not searched for.
.PP
.SH RETURN VALUE
.PP
.RS 4

.RE
.PP
If the operation was successful,
.ft B
mtext_prop_range()
.ft R
returns the number of values the property
.ft B
key
.ft R
has at pos. Otherwise it returns \-1 and assigns an error code to the external variable
.ft C
merror_code\fP.
.ft R
.PP
.SH ERRORS
.PP
.RS 4
\fCMERROR_RANGE\fP,
.ft R
.ft C
MERROR_SYMBOL
.ft R
.RE
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBmtext_put_prop()\fP,
.ft R
.ft B
mtext_put_prop_values()\fP,
.ft R
.ft B
mtext_get_prop()\fP,
.ft R
.ft B
mtext_get_prop_values()\fP,
.ft R
.ft B
mtext_pop_prop()\fP,
.ft R
.ft B
mtext_push_prop()
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
