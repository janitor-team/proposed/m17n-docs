.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mflt_get" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mflt_get \- Return an FLT object that has a specified name. 


.SH SYNOPSIS
\fBMFLT\fP *
\fBmflt_get\fP (\fBMSymbol\fP
\fIname\fP)


.SH DESCRIPTION
Return an FLT object that has a specified name. The
.ft B
mflt_get()
.ft R
function returns an FLT object whose name is
.ft B
name\fP.
.ft R
.PP
.SH RETURN VALUE
.PP
.RS 4
If the operation was successful,
.ft B
mflt_get()
.ft R
returns a pointer to the found FLT object. Otherwise, it returns
.ft C
NULL\fP.
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
