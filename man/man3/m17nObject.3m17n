.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "Managed Object" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
Managed_Object \- Objects managed by the reference count.  

.SS "Data Structures"

.in +1c
.ti -1c
.RI "struct \fBM17NObjectHead\fP"
.br
.RI "\fIThe first member of a managed object. \fP"
.in -1c
.SS "Functions"

.in +1c
.ti -1c
.RI "void * \fBm17n_object\fP (int size, void(*freer)(void *))"
.br
.RI "\fIAllocate a managed object. \fP"
.ti -1c
.RI "int \fBm17n_object_ref\fP (void *object)"
.br
.RI "\fIIncrement the reference count of a managed object. \fP"
.ti -1c
.RI "int \fBm17n_object_unref\fP (void *object)"
.br
.RI "\fIDecrement the reference count of a managed object. \fP"
.in -1c
.SH "Detailed Description"
.PP 
Objects managed by the reference count. 

Managed objects are objects managed by the reference count.
.PP
There are some types of m17n objects that are managed by their reference count. Those objects are called \fImanaged\fP \fIobjects\fP. When created, the reference count of a managed object is initialized to one. The \fBm17n_object_ref()\fP function increments the reference count of a managed object by one, and the \fBm17n_object_unref()\fP function decrements by one. A managed object is automatically freed when its reference count becomes zero.
.PP
A property whose key is a managing key can have only a managed object as its value. Some functions, for instance \fBmsymbol_put()\fP and \fBmplist_put()\fP, pay special attention to such a property.
.PP
In addition to the predefined managed object types, users can define their own managed object types. See the documentation of the \fBm17n_object()\fP for more details. 
.SH "Author"
.PP 
Generated automatically by Doxygen for The m17n Library from the source code.
.SH "Data Structure Documentation"
.SS M17NObjectHead
.PP
.ad l
.nh
.PP
\fBFIELD DOCUMENTATION:\fP
.PP 
.PP
\fBvoid* \fBM17NObjectHead::filler\fP[2]\fP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
