.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mface_put_prop" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mface_put_prop \- Set a value of a face property. 


.SH SYNOPSIS
int
\fBmface_put_prop\fP (\fBMFace\fP *
\fIface\fP, \fBMSymbol\fP
\fIkey\fP, void *
\fIval\fP)


.SH DESCRIPTION
Set a value of a face property. The
.ft B
mface_put_prop()
.ft R
function assigns
.ft B
val
.ft R
to the property whose key is
.ft B
key
.ft R
in face
.ft B
face\fP.
.ft R
.ft B
key
.ft R
must be one the followings:
.PP
\fBMforeground\fP,
.ft R
.ft B
Mbackground\fP,
.ft R
.ft B
Mvideomode\fP,
.ft R
.ft B
Mhline\fP,
.ft R
.ft B
Mbox\fP,
.ft R
.ft B
Mfoundry\fP,
.ft R
.ft B
Mfamily\fP,
.ft R
.ft B
Mweight\fP,
.ft R
.ft B
Mstyle\fP,
.ft R
.ft B
Mstretch\fP,
.ft R
.ft B
Madstyle\fP,
.ft R
.ft B
Msize\fP,
.ft R
.ft B
Mfontset\fP,
.ft R
.ft B
Mratio\fP,
.ft R
.ft B
Mhook_func\fP,
.ft R
.ft B
Mhook_arg
.ft R
.PP
Among them, font related properties (\fBMfoundry
.ft R
through
.ft B
Msize\fP) are used as the default values when a font in the fontset of
.ft B
face
.ft R
does not specify those values.
.PP
The actual type of the returned value depends of
.ft B
key\fP.
.ft R
See documentation of the above keys.
.PP
.SH RETURN VALUE
.PP
.RS 4
If the operation was successful,
.ft B
mface_put_prop()
.ft R
returns 0. Otherwise it returns \-1 and assigns an error code to the external variable
.ft B
merror_code\fP.
.ft R
.RE
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBmface_get_prop()
.ft R
.RE
.PP
.SH ERRORS
.PP
.RS 4
\fCMERROR_FACE
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
