.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mtext_line_break" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mtext_line_break \- Find a linebreak postion of an M\-text. 


.SH SYNOPSIS
int
\fBmtext_line_break\fP (\fBMText\fP *
\fImt\fP, int
\fIpos\fP, int
\fIoption\fP, int *
\fIafter\fP)


.SH DESCRIPTION
Find a linebreak postion of an M\-text. The
.ft B
mtext_line_break()
.ft R
function checks if position
.ft B
pos
.ft R
is a proper linebreak position of an M\-text
.ft B
mt
.ft R
according to the algorithm of The Unicode Standard 4.0 UAX#14. It so, it returns
.ft B
pos\fP.
.ft R
Otherwise, it returns a proper linebreak position before
.ft B
pos\fP.
.ft R
.PP
If
.ft B
option
.ft R
is nonzero, it controls the algorithm by logical\-or of the members of
.ft B
MTextLineBreakOption\fP.
.ft R
.PP
If
.ft B
after
.ft R
is not NULL, a proper linebreak position after
.ft B
pos
.ft R
is stored there. 
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
