.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mconv_encode" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mconv_encode \- Encode an M\-text into a byte sequence. 


.SH SYNOPSIS
int
\fBmconv_encode\fP (\fBMConverter\fP *
\fIconverter\fP, \fBMText\fP *
\fImt\fP)


.SH DESCRIPTION
Encode an M\-text into a byte sequence. The
.ft B
mconv_encode()
.ft R
function encodes M\-text
.ft B
mt
.ft R
and writes the resulting byte sequence into the buffer area or the stream that is currently bound to code converter
.ft B
converter\fP.
.ft R
.PP
.SH RETURN VALUE
.PP
.RS 4
If the operation was successful,
.ft B
mconv_encode()
.ft R
returns the number of written bytes. Otherwise it returns \-1 and assigns an error code to the external variable
.ft B
merror_code\fP.
.ft R
.RE
.PP
.SH ERRORS
.PP
.RS 4
\fCMERROR_IO\fP,
.ft R
.ft C
MERROR_CODING
.ft R
.RE
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBmconv_rebind_buffer()\fP,
.ft R
.ft B
mconv_rebind_stream()\fP,
.ft R
.ft B
mconv_decode()\fP,
.ft R
.ft B
mconv_encode_range()
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
