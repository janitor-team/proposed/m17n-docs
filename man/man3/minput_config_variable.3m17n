.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "minput_config_variable" 3m17n "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
minput_config_variable \- Configure the value of an input method variable. 


.SH SYNOPSIS
int
\fBminput_config_variable\fP (\fBMSymbol\fP
\fIlanguage\fP, \fBMSymbol\fP
\fIname\fP, \fBMSymbol\fP
\fIvariable\fP, \fBMPlist\fP *
\fIvalue\fP)


.SH DESCRIPTION
Configure the value of an input method variable. The
.ft B
minput_config_variable()
.ft R
function assigns
.ft B
value
.ft R
to the variable
.ft B
variable
.ft R
of the input method specified by
.ft B
language
.ft R
and
.ft B
name\fP.
.ft R
.PP
If
.ft B
value
.ft R
is a non\-empty plist, it must be a plist of one element whose key is
.ft B
Minteger\fP,
.ft R
.ft B
Msymbol\fP,
.ft R
or
.ft B
Mtext\fP,
.ft R
and the value is of the corresponding type. That value is assigned to the variable.
.PP
If
.ft B
value
.ft R
is an empty plist, any configuration and customization of the variable are canceled, and the default value is assigned to the variable.
.PP
If
.ft B
value
.ft R
is NULL, the configuration of the variable is canceled, and the original value (what saved in per\-user customization file, or the default value) is assigned to the variable.
.PP
In the latter two cases,
.ft B
variable
.ft R
can be
.ft B
Mnil
.ft R
to make all the variables of the input method the target of the operation.
.PP
If
.ft B
name
.ft R
is
.ft B
Mnil\fP,
.ft R
this function configures the value of global variable, not that of a specific input method.
.PP
The configuration takes effect for input methods opened or re\-opened later in the current session. To make the configuration take effect for the future session, it must be saved in a per\-user customization file by the function
.ft B
minput_save_config()\fP.
.ft R
.PP
.SH RETURN VALUE
.PP
.RS 4

.RE
.PP
If the operation was successful, this function returns 0, otherwise returns \-1. The operation fails in these cases: 
.PD 0

.IP "\(bu" 2
\fBvalue
.ft R
is not in a valid form, the type does not match the definition, or the value is our of range. 
.IP "\(bu" 2
\fBvariable
.ft R
is not available for the input method. 
.IP "\(bu" 2
\fBlanguage
.ft R
and
.ft B
name
.ft R
do not specify an existing input method. 
.PP
.PP
.SH "SEE ALSO"
.PP
.RS 4
\fBminput_get_variable()\fP,
.ft R
.ft B
minput_save_config()\fP.
.ft R
.RE
.PP

.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
