.\" Copyright (C) 2001 Information-technology Promotion Agency (IPA)
.\" Copyright (C) 2001-2011
.\"   National Institute of Advanced Industrial Science and Technology (AIST)
.\" This file is part of the m17n library documentation.
.\" Permission is granted to copy, distribute and/or modify this document
.\" under the terms of the GNU Free Documentation License, Version 1.2 or
.\" any later version published by the Free Software Foundation; with no
.\" Invariant Section, no Front-Cover Texts,
.\" and no Back-Cover Texts.  A copy of the license is included in the
.\" appendix entitled "GNU Free Documentation License".
.TH "mdbFLT" 5 "12 Jan 2011" "Version 1.6.2" "The m17n Library" \" -*- nroff -*-
.ad l
.nh
.SH NAME
mdbFLT \- Font Layout Table 
.SH "DESCRIPTION"
.PP
For simple scripts, the rendering engine converts character codes into glyph codes one by one by consulting the encoding of each selected font. But, to render text that requires complicated layout (e.g. Thai and Indic scripts), one to one conversion is not sufficient. A sequence of characters may have to be drawn as a single ligature. Some glyphs may have to be drawn at 2\-dimensionally shifted positions.
.PP
To handle those complicated scripts, the m17n library uses Font Layout Tables (FLTs for short). The FLT driver interprets an FLT and converts a character sequence into a glyph sequence that is ready to be passed to the rendering engine.
.PP
An FLT can contain information to extract a grapheme cluster from a character sequence and to reorder the characters in the cluster, in addition to information found in OpenType Layout Tables (CMAP, GSUB, and GPOS).
.PP
An FLT is a cascade of one or more conversion stages. In each stage, a sequence is converted into another sequence to be read in the next stage. The length of sequences may differ from stage to stage. Each element in a sequence has the following integer attributes.
.PP
.PD 0
.IP "\(bu" 2
code
.PP
In the first conversion stage, this is the character code in the original character sequence. In the last stage, it is the glyph code passed to the rendering engine. In other cases, it is an intermediate glyph code.
.PP

.IP "\(bu" 2
category
.PP
The category code defined in the \fCCATEGORY\-TABLE\fP of the current stage, or defined in the one of the former stages and not overwritten by later stages.
.PP

.IP "\(bu" 2
combining\-spec
.PP
If nonzero, it specifies how to combine this (intermediate) glyph with the previous one.
.PP

.IP "\(bu" 2
left\-padding\-flag
.PP
If nonzero, it instructs the rendering function to insert a padding space before this (intermediate) glyph so that the glyph does not overlap with the previous one.
.PP

.IP "\(bu" 2
right\-padding\-flag
.PP
If nonzero, it instructs the rendering function to insert a padding space after this (intermediate) glyph so that the glyph does not overlap with the next one.
.PP

.PP
.PP
When the layout engine draws text, it at first determines a font and an FLT for each character in the text. For each subsequence of characters that use the same font and FLT, the layout engine generates a corresponding intermediate glyph sequence. The code attribute of each element in the intermediate glyph sequence is its character code, and all other attributes are zeros. This sequence is processed in the first stage of FLT as the current \fIrun\fP (substring).
.PP
Each stage works as follows.
.PP
At first, if the stage has a \fCCATEGORY\-TABLE\fP, the category of each glyph in the current run is updated. If there is a glyph that has no category, the current run ends before that glyph.
.PP
Then, the default values of code\-offset, combining\-spec, and left\-padding\-flag of this stage are initialized to zero.
.PP
Next, the initial conversion rule of the stage is applied to the current run.
.PP
Lastly, the current run is replaced with the newly produced (intermediate) glyph sequence.
.SH "SYNTAX and SEMANTICS"
.PP
The m17n library loads an FLT from the m17n database using the tag <font, layouter, FLT\-NAME>. The date format of an FLT is as follows:
.PP
.PP
.nf

FONT\-LAYOUT\-TABLE ::= FLT\-DECLARATION ? STAGE0 STAGE *

FLT\-DECLARATION ::= '(' 'font' 'layouter' FLT\-NAME nil PROP * ')'
FLT\-NAME ::= SYMBOL
PROP :: = VERSION | FONT
VERSION ::= '(' 'version' MTEXT ')'
FONT ::= '(' 'font' FONT\-SPEC ')'
FONT\-SPEC ::=
     '(' [[ FOUNDRY FAMILY
           [ WEIGHT [ STYLE [ STRETCH [ ADSTYLE ]]]]]
         REGISTRY ]
	 [ OTF\-SPEC ] [ LANG\-SPEC ] ')'

STAGE0 ::= CATEGORY\-TABLE GENERATOR

STAGE ::= CATEGORY\-TABLE ? GENERATOR

CATEGORY\-TABLE ::= '(' 'category' CATEGORY\-SPEC + ')'

CATEGORY\-SPEC ::= '(' CODE CATEGORY ')'
                  | '(' CODE CODE CATEGORY ')'

CODE ::= INTEGER

CATEGORY ::= INTEGER
.fi
.PP
.PP
In the definition of \fCCATEGORY\-SPEC\fP, \fCCODE\fP is a glyph code, and \fCCATEGORY\fP is ASCII code of an upper or lower letter, i.e. one of 'A', ... 'Z', 'a', .. 'z'.
.PP
The first form of \fCCATEGORY\-SPEC\fP assigns \fCCATEGORY\fP to a glyph whose code is \fCCODE\fP. The second form assigns \fCCATEGORY\fP to glyphs whose code falls between the two \fCCODEs\fP.
.PP
.PP
.nf

GENERATOR ::= '(' 'generator' RULE MACRO\-DEF * ')'

RULE ::= REGEXP\-BLOCK | MATCH\-BLOCK | SUBST\-BLOCK | COND\-BLOCK
         FONT\-FACILITY\-BLOCK | DIRECT\-CODE | COMBINING\-SPEC | OTF\-SPEC
         | PREDEFINED\-RULE | MACRO\-NAME

MACOR\-DEF ::= '(' MACRO\-NAME RULE + ')'
.fi
.PP
.PP
Each \fCRULE\fP specifies glyphs to be consumed and glyphs to be produced. When some glyphs are consumed, they are taken away from the current run. A rule may fail in some condition. If not described explicitly to fail, it should be regarded that the rule succeeds.
.PP
.PP
.nf

DIRECT\-CODE ::= INTEGER
.fi
.PP
.PP
This rule consumes no glyph and produces a glyph which has the following attributes:
.PP
.PD 0
.IP "\(bu" 2
code : \fCINTEGER\fP plus the default code\-offset 
.IP "\(bu" 2
combining\-spec : default value 
.IP "\(bu" 2
left\-padding\-flag : default value 
.IP "\(bu" 2
right\-padding\-flag : zero 
.PP
.PP
After having produced the glyph, the default code\-offset, combining\-spec, and left\-padding\-flag are all reset to zero.
.PP
.PP
.nf

PREDEFINED\-RULE ::= '=' | '*' | '<' | '>' | '|' | '[' | ']'
.fi
.PP
.PP
They perform actions as follows.
.PP
.PD 0
.IP "\(bu" 2
\fC=\fP 
.PP
This rule consumes the first glyph in the current run and produces the same glyph. It fails if the current run is empty.
.PP

.IP "\(bu" 2
\fC*\fP 
.PP
This rule repeatedly executes the previous rule. If the previous rule fails, this rule does nothing and fails.
.PP

.IP "\(bu" 2
\fC<\fP 
.PP
This rule specifies the start of a grapheme cluster.
.PP

.IP "\(bu" 2
\fC>\fP 
.PP
This rule specifies the end of a grapheme cluster.
.PP

.IP "\(bu" 2
\fC@\fP[
.PP
This rule sets the default left\-padding\-flag to 1. No glyph is consumed. No glyph is produced.
.PP

.IP "\(bu" 2
\fC@\fP]
.PP
This rule changes the right\-padding\-flag of the lastly generated glyph to 1. No glyph is consumed. No glyph is produced.
.PP

.IP "\(bu" 2
\fC\fP|
.PP
This rule consumes no glyph and produces a special glyph whose category is ' ' and other attributes are zero. This is the only rule that produces that special glyph.
.PP

.PP
.PP
.PP
.nf

REGEXP\-BLOCK ::= '(' REGEXP RULE * ')'

REGEXP ::= MTEXT
.fi
.PP
.PP
\fCMTEXT\fP is a regular expression that should match the sequence of categories of the current run. If a match is found, this rule executes \fCRULEs\fP temporarily limiting the current run to the matched part. The matched part is consumed by this rule.
.PP
Parenthesized subexpressions, if any, are recorded to be used in \fCMATCH\-BLOCK\fP that may appear in one of \fCRULEs\fP.
.PP
If no match is found, this rule fails.
.PP
.PP
.nf

MATCH\-BLOCK ::= '(' MATCH\-INDEX RULE * ')'

MATCH\-INDEX ::= INTEGER
.fi
.PP
.PP
\fCMATCH\-INDEX\fP is an integer specifying a parenthesized subexpression recorded by the previous \fCREGEXP\-BLOCK\fP. If such a subexpression was found by the previous regular expression matching, this rule executes \fCRULEs\fP temporarily limiting the current run to the matched part of the subexpression. The matched part is consumed by this rule.
.PP
If no match was found, this rule fails.
.PP
If this is the first rule of the stage, \fCMATCH\-INDEX\fP must be 0, and it matches the whole current run.
.PP
.PP
.nf

SUBST\-BLOCK ::= '(' SOURCE\-PATTERN RULE * ')'

SOURCE\-PATTERN ::= '(' CODE + ')'
                   | (' 'range' CODE CODE ')'
.fi
.PP
.PP
If the sequence of codes of the current run matches \fCSOURCE\-PATTERN\fP, this rule executes \fCRULEs\fP temporarily limiting the current run to the matched part. The matched part is consumed.
.PP
The first form of \fCSOURCE\-PATTERN\fP specifies a sequence of glyph codes to be matched. In this case, this rule resets the default code\-offset to zero.
.PP
The second form specifies a range of codes that should match the first glyph code of the code sequence. In this case, this rule sets the default code\-offset to the first glyph code minus the first \fCCODE\fP specifying the range.
.PP
If no match is found, this rule fails.
.PP
.PP
.nf

FONT\-FACILITY\-BLOCK ::= '(' FONT\-FACILITY RULE * ')'
FONT\-FACILITY = '(' 'font\-facility' CODE * ')'
	      	| '(' 'font\-facility' FONT\-SPEC ')'
.fi
.PP
.PP
If the current font has glyphs for \fCCODEs\fP or matches with \fCFONT\-SPEC\fP, this rule succeeds and \fCRULEs\fP are executed. Otherwise, this rule fails.
.PP
.PP
.nf

COND\-BLOCK ::= '(' 'cond' RULE + ')'
.fi
.PP
.PP
This rule sequentially executes \fCRULEs\fP until one succeeds. If no rule succeeds, this rule fails. Otherwise, it succeeds.
.PP
 
.PP
.nf

OTF\-SPEC ::= SYMBOL

.fi
.PP
.PP
\fCOTF\-SPEC\fP is a symbol whose name specifies an instruction to the OTF driver. The name has the following syntax.
.PP
.PP
.nf

  OTF\-SPEC\-NAME ::= ':otf=' SCRIPT LANGSYS ? GSUB\-FEATURES ? GPOS\-FEATURES ?

  SCRIPT ::= SYMBOL

  LANGSYS ::= '/' SYMBOL

  GSUB\-FEATURES ::= '=' FEATURE\-LIST ?

  GPOS\-FEATURES ::= '+' FEATURE\-LIST ?

  FEATURE\-LIST ::= ( SYMBOL ',' ) * [ SYMBOL | '*' ]

.fi
.PP
.PP
Each \fCSYMBOL\fP specifies a tag name defined in the OpenType specification.
.PP
For \fCSCRIPT\fP, \fCSYMBOL\fP specifies a Script tag name (e.g. deva for Devanagari).
.PP
For \fCLANGSYS\fP, \fCSYMBOL\fP specifies a Language System tag name. If \fCLANGSYS\fP is omitted, the Default Language System table is used.
.PP
For \fCGSUB\-FEATURES\fP, each \fCSYMBOL\fP in \fCFEATURE\-LIST\fP specifies a GSUB Feature tag name to apply. '*' is allowed as the last item to specify all remaining features. If \fCSYMBOL\fP is preceded by '~' and the last item is '*', \fCSYMBOL\fP is excluded from the features to apply. If no \fCSYMBOL\fP is specified, no GSUB feature is applied. If \fCGSUB\-FEATURES\fP itself is omitted, all GSUB features are applied.
.PP
When \fCOTF\-SPEC\fP appears in a \fCFONT\-SPEC\fP, \fCFEATURE\-LIST\fP specifies features that the font must have (or must not have if preceded by '~'), and the last'*', even if exists, has no meaning.
.PP
The specification of \fCGPOS\-FEATURES\fP is analogous to that of \fCGSUB\-FEATURES\fP.
.PP
Please note that all the tags above must be 4 ASCII printable characters.
.PP
See the following page for the OpenType specification.
.br
 <http://www.microsoft.com/typography/otspec/default.htm>
.PP
.PP
.nf

COMBINING ::= SYMBOL
.fi
.PP
.PP
\fCCOMBINING\fP is a symbol whose name specifies how to combine the next glyph with the previous one. This rule sets the default combining\-spec to an integer code that is unique to the symbol name. The name has the following syntax.
.PP
.PP
.nf

  COMBINING\-NAME ::= VPOS HPOS OFFSET VPOS HPOS

  VPOS ::= 't' | 'c' | 'b' | 'B'

  HPOS ::= 'l' | 'c' | 'r'

  OFFSET :: = '.' | XOFF | YOFF XOFF ?

  XOFF ::= ('<' | '>') INTEGER ?

  YOFF ::= ('+' | '\-') INTEGER ?
.fi
.PP
.PP
\fCVPOS\fP and \fCHPOS\fP specify the vertical and horizontal positions as described below.
.PP
.PP
.nf

                                POINT VPOS HPOS
                                \-\-\-\-\- \-\-\-\- \-\-\-\-
    0\-\-\-\-1\-\-\-\-2 <\-\-\-\- top       0     t    l
    |         |                 1     t    c
    |         |                 2     t    r
    |         |                 3     B    l
    9   10   11 <\-\-\-\- center    4     B    c
    |         |                 5     B    r
  \-\-3\-\-\-\-4\-\-\-\-5\-\- <\-\- baseline  6     b    l
    |         |                 7     b    c
    6\-\-\-\-7\-\-\-\-8 <\-\-\-\- bottom    8     b    r
                                9     c    l
    |    |    |                10     c    c
  left center right            11     c    r
.fi
.PP
.PP
The left figure shows 12 reference points of a glyph by numbers 0 to 11. The rectangle 0\-6\-8\-2 is the bounding box of the glyph, the positions 3, 4, and 5 are on the baseline, 9\-11 are on the vertical center of the box, 0\-2 and 6\-8 are on the top and on the bottom respectively. 1, 10, 4, and 7 are on the horizontal center of the box.
.PP
The right table shows how those reference points are specified by a pair of \fCVPOS\fP and \fCHPOS\fP.
.PP
The first \fCVPOS\fP and \fCHPOS\fP in the definition of \fCCOMBINING\-NAME\fP specify the reference point of the previous glyph, and the second \fCVPOS\fP and \fCHPOS\fP specify that of the next glyph. The next glyph is drawn so that these two reference points align.
.PP
\fCOFFSET\fP specifies the way of alignment in detail. If it is '.', the reference points are on the same position.
.PP
\fCXOFF\fP specifies how much the X position of the reference point of the next glyph should be shifted to the left ('<') or right ('>') from the previous reference point.
.PP
\fCYOFF\fP specifies how much the Y position of the reference point the next glyph should be shifted upward ('+') or downward ('\-') from the previous reference point.
.PP
In both cases, \fCINTEGER\fP is the amount of shift expressed as a percentage of the font size, i.e., if \fCINTEGER\fP is 10, it means 10% (1/10) of the font size. If \fCINTEGER\fP is omitted, it is assumed that 5 is specified.
.PP
Once the next glyph is combined with the previous one, they are treated as a single combined glyph.
.PP
.PP
.nf

MACRO\-NAME ::= SYMBOL
.fi
.PP
.PP
\fCMACRO\-NAME\fP is a symbol that appears in one of \fCMACRO\-DEF\fP. It is exapanded to the sequence of the corresponding \fCRULEs\fP.
.SH "CONTEXT DEPENDENT BEHAVIOR"
.PP
So far, it has been assumed that each sequence, which is drawn with a specific font, is context free, i.e. not affected by the glyphs preceding or following that sequence. This is true when sequence S1 is drawn with font F1 while the preceding sequence S0 unconditionally requires font F0.
.PP
.PP
.nf

  sequence                              S0      S1
  currently used font                   F0      F1
  usable font(s)                        F0      F1
.fi
.PP
.PP
Sometimes, however, a clear separation of sequences is not possible. Suppose that the preceding sequence S0 can be drawn not only with F0 but also with F1.
.PP
.PP
.nf

  sequence                              S0      S1
  currently used font                   F0      F1
  usable font(s)                        F0,F1   F1
.fi
.PP
.PP
In this case, glyphs used to draw the preceding S0 may affect glyph generation of S1. Therefore it is necessary to access information about S0, which has already been processed, when processing S1. Generation rules in the first stage (only in the first stage) accept a special regular expression to access already processed parts.
.PP
.PP
.nf

  "RE0 RE1"
.fi
.PP
.PP
\fCRE0\fP and \fCRE1\fP are regular expressions that match the preceding sequence S0 and the following sequence S1, respectively.
.PP
Pay attention to the space between the two regular expressions. It represents the special category ' ' (see above). Note that the regular expression above belongs to glyph generation rules using font F1, therefore not only RE1 but also RE0 must be expressed with the categories for F1. This means when the preceding sequence S0 cannot be expressed with the categories for F1 (as in the first example above) generation rules having these patterns never match.
.SH "SEE ALSO"
.PP
\fBmdbGeneral(5)\fP, \fBFLTs provided by the m17n database\fP 
.SH COPYRIGHT
Copyright (C) 2001 Information\-technology Promotion Agency (IPA)
.br
Copyright (C) 2001\-2011 National Institute of Advanced Industrial Science and Technology (AIST)
.br
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License 
<http://www.gnu.org/licenses/fdl.html>.
